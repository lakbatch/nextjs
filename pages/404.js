import React from 'react';
import DefaultLayout from "../Components/LayoutComponents/DefaultLayout";

export default function Custom404(props) {
    const PageMarkup = (
        <>
            {/* {props.Errortype === "Error404" ? */}
            <img src="./Assets/Images/error404.svg" className="error-img" alt="error" />
            {/* : ""} */}
        </>
    );

    return <DefaultLayout View={PageMarkup} />
}
