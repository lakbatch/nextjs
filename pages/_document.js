import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head>
                    
                    <meta name="theme-color" content="#3884bd" />
                    <meta property="og:site_name" content="Batchleads" />
                    <meta property="og:type" content="website" />
                    <meta name="msvalidate.01" content="C5EBBE4D17352190E3B77E86515821BD" />
                    <link rel="manifest" href="/manifest.json" />
                    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
                    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" async />
                    <link rel="apple-touch-icon" href="%PUBLIC_URL%/favicon.png" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,500;1,700;1,800&family=Bebas+Neue&display=swap" rel="stylesheet" />
                    <script src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
                    <script 
                        type="application/ld+json"
                        dangerouslySetInnerHTML={ { __html: `
                        {
                            "@context": "https://schema.org",
                            "@type": "Corporation",
                            "name": "Batchleads",
                            "url": "https://batchleads.io/",
                            "logo": "https://batchleads.io/static/media/logo.e614377f.svg",
                            "sameAs": [
                                "https://www.facebook.com/groups/batchservice/",
                                "https://www.instagram.com/batch_leads/"
                            ]
                        }
                        `}}
                        >
                    </script>
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument