import React, { useState, useEffect } from "react";
import { TabContent } from "react-bootstrap";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tabs';
// import ListFilter from "../../Assets/Images/Propertiesfilter.svg";
// import MapSerach from "../../Assets/Images/Map-search.svg";
// import SkipTracing from "../../Assets/Images/Skiptracing.svg";
// import UnRead from "../../Assets/Images/unreadmsgs.svg";
import CTAButons from "./CTAButons";


const HomeTabs = () => {


  const MakeCarousel = () => {
    let MapTab = document.getElementById('home-tabs-tab-map');
    let ListTab = document.getElementById('home-tabs-tab-list');
    let SkipTab = document.getElementById('home-tabs-tab-Skip');
    let MessagingTab = document.getElementById('home-tabs-tab-messaging');
    if (MapTab !== null && ListTab !== null && SkipTab !== null && MessagingTab !== null) {

      setTimeout(() => {
        MapTab.click();
      }, 200);
      setTimeout(() => {
        ListTab.click();
      }, 1000);
      setTimeout(() => {
        SkipTab.click();
      }, 4000);
      setTimeout(() => {
        MessagingTab.click();
      }, 8000);

    }
  }



  const [Inter, SetInter] = useState(11000);

  useEffect(() => {

    if (Inter !== null) {
      let id = setInterval(function () { MakeCarousel() }, Inter);
      return () => clearInterval(id);
    }
  }, [Inter]);



  return (
    <>
      <Tabs defaultActiveKey="map" transition={false} id="home-tabs" /*onSelect={() => SetTabState(true)}*/ >
        <Tab eventKey="map" title="Map Search" >

          <TabContent>
            <div className="tabs-img" data-aos="fade-right" data-aos-duration="1200">
              <img src="./Assets/Images/Map-search.svg" alt="map" /></div>
            <div className="table-info" data-aos="fade-left" data-aos-duration="1200">
              <h4 className=" title-tab mb-3"><span className="blue_text font_med ">Map Search</span></h4>

              <p className="font_18">Nationwide sellers and buyers lists built within our platform. <span className="font_bold black_text">Batch<span className="blue_text">Leads</span></span> has made it more simple than ever to target Motivated Prospects.</p>
              <div className="mt-5 first_btn">
                <CTAButons></CTAButons></div>
            </div>
          </TabContent>
        </Tab>
        <Tab eventKey="list" title="list manager">
          <TabContent>
            <div className="tabs-img" data-aos="fade-right" data-aos-duration="1200">
              <img src="./Assets/Images/Propertiesfilter.svg" alt="list" /></div>
            <div className="table-info" data-aos="fade-left" data-aos-duration="1200">
              <h4 className=" title-tab mb-3"><span className="blue_text font_med ">List Manager</span></h4>

              <p className="font_18"> <span className="font_bold black_text">Batch<span className="blue_text">Leads</span></span> helps you organize and manage your lists. Stop wasting time and money targeting the wrong people. Say Goodbye to Excel Spreadsheets FOREVER.</p>
              <div className="mt-3">
                <CTAButons></CTAButons></div>
            </div>
          </TabContent>
        </Tab>
        <Tab eventKey="Skip" title="Skip Tracing">
          <TabContent>
            <div className="tabs-img" data-aos="fade-right" data-aos-duration="1200">
              <img src="./Assets/Images/Skiptracing.svg" alt="skip" /></div>
            <div className="table-info" data-aos="fade-left" data-aos-duration="1200">
              <h4 className=" title-tab mb-3"><span className="blue_text font_med ">Skip Tracing</span></h4>

              <p className="font_18">It’s the process of locating an individuals most recent contact information. Sometimes it includes multiple phone numbers and the <span className="orange_text font_bold">Golden Address</span>. We give you a competitive advantage by only using tier-one data, and you only pay for successful matches!</p>
              <div className="mt-3">
                <CTAButons></CTAButons></div>
            </div>
          </TabContent>
        </Tab>
        <Tab eventKey="messaging" title="Text messaging">
          <TabContent>
            <div className="tabs-img" data-aos="fade-right" data-aos-duration="1200">
              <img src="./Assets/Images/unreadmsgs.svg" alt="sms" /></div>
            <div className="table-info" data-aos="fade-left" data-aos-duration="1200">
              <h4 className=" title-tab mb-3"><span className="blue_text font_med ">Text Messaging</span></h4>

              <p className="font_18"> It’s the best way to get in touch with your leads. Deliverability and response rates are off the charts. We’ve built an easy, affordable, fully-compliant texting platform that you’re going to love!</p>
              <div className="mt-3">
                <CTAButons></CTAButons></div>
            </div>
          </TabContent>
        </Tab>

      </Tabs>
    </>
  )
}

export default HomeTabs;