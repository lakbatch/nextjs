import React, { useState } from "react";
import DefaultLayout from '../Components/LayoutComponents/DefaultLayout';
import Head from 'next/head'
// import UserIcon from "../../Assets/Images/user.svg";
// import EmailIcon from "../../Assets/Images/email.svg";
// import CommentIcon from "../../Assets/Images/pi-chat.svg";
import TrustpilotReviews from "../Components/GlobalComponents/TrustpilotReviews";
import ZapierForm from 'react-zapier-form';



const PersonalInformation = () => {
    const [Firstname, SetFirstname] = useState("");
    const [Lastname, SetLastname] = useState("");
    const [Email, SetEmail] = useState("");
    const [Comments, SetComments] = useState("");



    const PageMarkup = (
        <>
            <section className="inner_bnr d-flex align-items-center">
                <div className="container text-center w-100">
                    <h1 className="head_h1 font_bebas blue_text font_reg" data-aos="fade-up" data-aos-duration="1200">REMOVAL <span className="black_text">REQUEST</span></h1>
                </div>
            </section>
            <section className="form_sec removal inner_dots">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4 pt-5 pp-p" data-aos="fade-right" data-aos-duration="1200">
                            <p className="black_text">Your personal privacy is our highest priority and we are happy accommodate your request to remove your personal information from our services.</p>
                            <p className="black_text">We may need to reach out to you directly via email to verify your request and assure that it will be processed safely and accurately. Please check your email for correspondence over the next few days.</p>
                        </div>

                        <div className="col-md-6 leftDot_line" data-aos="fade-left" data-aos-duration="1200">
                            <div className="form_box col-md-12 pt-2">

                                <ZapierForm action='https://hooks.zapier.com/hooks/catch/2424086/onmtyix/'>
                                    {({ error, loading, success }) => {
                                        return (
                                            <div>
                                                {!success && !loading &&
                                                    <div>
                                                        <div className="form-group">
                                                            <input type="text" value={Firstname} onChange={(e) => SetFirstname(e.target.value)} className="form-control" placeholder="First Name*" id="fname" name="fname" required />
                                                            <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                                            <span id="error_name"></span>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="text" value={Lastname} onChange={(e) => SetLastname(e.target.value)} className="form-control" placeholder="Last Name*" name="lname" id="lname" required />
                                                            <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                                            <span id="error_lastname"></span>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="email" value={Email} onChange={(e) => SetEmail(e.target.value)} className="form-control" placeholder="Email*" name="email" id="email" required />
                                                            <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/email.svg" alt="email" /></div>
                                                            <span id="error_email"></span>
                                                        </div>
                                                        <div className="form-group">
                                                            <textarea className="form-control" onChange={(e) => SetComments(e.target.value)} value={Comments} rows="3" placeholder="Comment" name="comment" ></textarea>
                                                            <div className="form_icon comment_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/pi-chat.svg" alt="comment" /></div>
                                                        </div>
                                                        <div className="text-center">
                                                            <button type="submit" className="btn blue_btn">REMOVE MY DATA</button>

                                                        </div>
                                                    </div>
                                                }
                                                {loading && <div>Loading...</div>}
                                                {error && <div className="error_msg text-center"><p className="text_black font_bold text_18">Something went wrong. Please try again later.</p></div>}
                                                {success && <div className="error_msg text-center"><h1 className="head_h2 font_24 font_bold  ">Thank you! </h1><p className="text_black font_bold text_18 text-center">We will be in touch with you shortly.</p></div>}
                                            </div>
                                        )
                                    }}
                                </ZapierForm>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <TrustpilotReviews></TrustpilotReviews>
        </>
    );
    return (
        <>
            <Head>
                <title>Personal Information Removal Request | Batch Leads</title>
                <meta name="description" content="This form allows the user to apply for a personal information removal request with Batch Leads. This is a request for removing all data that we have on you." />
                <meta name="keywords" content="Personal Information Removal Request" />
                <link rel="canonical" href="https://batchleads.io/personal-information" />
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
            </Head>

            <DefaultLayout View={PageMarkup} />
        </>
    )
}

export default PersonalInformation;