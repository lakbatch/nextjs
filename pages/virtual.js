import React from "react";
import Link from "next/link";
import Head from "next/head";


import { RegisterButtonAffiliate } from "../Components/GlobalComponents/CTAButons";
import AffiHeader from "../Components/GlobalComponents/AffiliateHeader";
import AffiliateLayout from "../Components/LayoutComponents/AffiliateLayout";

// import TopDesk from "../../../Assets/Images/top-desk.svg";
// import Logo from "../../../Assets/Images/logo.svg";

const Virtual = () => {
  const AffiliateFPID = "virtual";

  return (
    <>
    <Head>
      <title>5000 Free Property Records LAUREN | BatchLeads</title>
      <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
      <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
    </Head>
      <main className="main_content">
        <section className="header_top pb-0">
          <div className="container text-center position-relative">
            <Link className="logo_top" href="/">
              <img src="./Assets/Images/logo.svg" alt="logo" />
            </Link>
          </div>
          <div className="banner_sec mt-5">
            <div className="container">
              <div className="row align-item-center justify-content-center">
                <div
                  className="col-md-6 aos-init aos-animate"
                  data-aos="fade-right"
                  data-aos-duration="1200"
                >
                  <AffiHeader></AffiHeader>
                  <RegisterButtonAffiliate FPID={AffiliateFPID} />{" "}
                </div>
                <div
                  className="col-md-6 text-right demo_video aos-init aos-animate"
                  data-aos="fade-left"
                  data-aos-duration="1200"
                >
                  <div className="tophero" style={{ width: "auto" }}>
                    <img className="extra_bg_hero" src="./Assets/Images/top-desk.svg" alt="hero" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <AffiliateLayout FPID={AffiliateFPID} />
      </main>
    </>
  );
};

export default Virtual;
