import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
// import Corey from "../../Assets/Images/corey.jpg";
// import AdamImg from "../../Assets/Images/adnam.jpg";
// import BrianImg from "../../Assets/Images/brian.jpg";
// import TerryImg from "../../Assets/Images/testi.png";
// import Derrick from "../../Assets/Images/Derrick.jpg";
// import BenImg from "../../Assets/Images/ben.png";
// import CassiImg from "../../Assets/Images/client2.svg";

// import CodyImg from "../../Assets/Images/cody.png";

import TestimonialModal from '../GlobalComponents/TestimonialModal'

const Testimonialslider = () => {
    const Settings = {
        dots: true,
        infinite: 1,
        loop: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        rows: 1,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                },
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
        ],
    };
    return (
        <section className="testimonial_sec">
            <div className="container text-center">
                <div className="testimonial_head">
                    <h2 className="head_h2 font_med white_text m-0">What Our Customers Have To Say</h2>
                    <p>See why so many are joining us everyday! </p>
                </div>
                <div className="testimonial_slide">
                    <div className="category">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="testimonial_slide">
                                    <Slider {...Settings}>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/Derrick.jpg" alt="User-img" className="userst" /></span>

                                                    <TestimonialModal Video={<iframe title="Derrick" className="Videomoadl" width="100%" height="515" src="https://www.youtube.com/embed/CIzwlhNMuxI?rel=0&autoplay=1&mute=1" frameBorder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>} />
                                                </div>
                                                <p>Sloane uses BatchLeads combined with Batch Skip Tracing and absolutely crushes his market!!</p>
                                                <h4 className="blue_text font_bold m-0">Sloane Johnson</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/corey.jpg" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Corey" width="100%" height="515" src="https://www.youtube.com/embed/NsvkawT9-N4?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Willie uses BatchLeads to it’s potential and it shows.. He is able to stay on top of his potential sellers and spend his time wisely!</p>
                                                <h4 className="blue_text font_bold m-0">Willie Lampkin</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/adnam.jpg" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Adam" width="100%" height="515" src="https://www.youtube.com/embed/RWdWjtTm0Lg?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Greg uses BatchLeads to help pinpoint vacant properties & manage his lists. Data Is King so use the service trusted by the pro’s!</p>
                                                <h4 className="blue_text font_bold m-0">Greg Helbeck</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/brian.jpg" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Brian" width="100%" height="515" src="https://www.youtube.com/embed/cHTdazR7O1g?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Alex shares how BatchLeads has helped him stay ahead of the competition.</p>
                                                <h4 className="blue_text font_bold m-0">Alex Shares</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/testi.png" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Terry" width="100%" height="515" src="https://www.youtube.com/embed/aYhwIkHUwGU?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Anthony uses BatchLeads to pull real estate lists, skip trace and text customers or potential sellers. It’s a one-stop shop!</p>
                                                <h4 className="blue_text font_bold m-0">Anthony Pappas</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/ben.png" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Video Ben" width="100%" height="515" src="https://www.youtube.com/embed/M_WuA8KUsXY?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Alec shares how BatchLeads has simplified his life. He was able to cut out multiple other subscriptions because he can do everything in one with BatchLeads. It’s a one-stop shop platform!</p>
                                                <h4 className="blue_text font_bold m-0">Alec Lebeck</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/client2.svg" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Ismael" width="100%" height="515" src="https://www.youtube.com/embed/PtsGAKRns0M?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Cassi shares how her business has become more efficient with BatchLeads!</p>
                                                <h4 className="blue_text font_bold m-0">Cassi DeHaas</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>
                                        <div className="items_custom">
                                            <div className="item_content pro_padding">
                                                <div className="user_img">
                                                    <span><img src="./Assets/Images/cody.png" alt="User-img" className="userst" /></span>
                                                    <TestimonialModal Video={<iframe title="Brent" Brent width="932" height="515" src="https://www.youtube.com/embed/wV_xi6YWF2E?feature=oembed" frameBorder="0" allow="accelerometer;  clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>} />
                                                </div>
                                                <p>Cody shares how BatchLeads took his business to the next level!</p>
                                                <h4 className="blue_text font_bold m-0">Cody Barton</h4>
                                                <h4 className="black_text m-0">CLIENT</h4>
                                            </div>
                                        </div>

                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Testimonialslider;