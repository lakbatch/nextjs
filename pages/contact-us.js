import React, { useState } from "react";
import Head from 'next/head';
import ZapierForm from "react-zapier-form";

import DefaultLayout from '../Components/LayoutComponents/DefaultLayout';
// import UserIcon from "../../Assets/Images/user.svg";
// import EmailIcon from "../../Assets/Images/email.svg";
// import PhoneIcon from "../../Assets/Images/phone.svg";
// import CommentIcon from "../../Assets/Images/pi-chat.svg";
import TrustpilotReviews from "../Components/GlobalComponents/TrustpilotReviews";


function Contact() {
    const [Firstname, SetFirstname] = useState("");
    const [Lastname, SetLastname] = useState("");
    const [Email, SetEmail] = useState("");
    const [Comments, SetComments] = useState("");

    const maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    const PageMarkup = (
        <>

            <section className="inner_bnr d-flex align-items-center">
                <div className="container text-center w-100">
                    <h1 className="head_h1 font_bebas blue_text font_reg" data-aos="fade-up" data-aos-duration="1200">Contact <span className="black_text">Us</span></h1>
                </div>
            </section>
            <section className="form_sec  inner_dots">
                <div className="container">
                    <div className="row align-items-center">
                        {<div className="col-md-5 text-center" data-aos="fade-right" data-aos-duration="1200">
                            <h3 className="black_text font_bold text-center blue_text">GET IN TOUCH <span style={{ color: "#24356C" }}> WITH US </span></h3>
                            <img src="./Assets/Images/contact_img.svg" className="contactus" alt="contat" />
                        </div>}

                        <div className="col-md-6 m-auto leftDot_line " data-aos="fade-left" data-aos-duration="1200">
                            <div className="form_box">
                                <ZapierForm action='https://hooks.zapier.com/hooks/catch/2424086/onm6vp1/'>
                                    {({ error, loading, success }) => {
                                        return (
                                            <div>
                                                {!success && !loading &&
                                                    <div>
                                                        <div className="row">
                                                            <div className="form-group col-md-12">
                                                                <input type="text" value={Firstname} onChange={(e) => SetFirstname(e.target.value)} className="form-control" name="fname" placeholder="First Name*" id="fname" required />
                                                                <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                                                <span id="error_name"></span>
                                                            </div>
                                                            <div className="form-group col-md-12">
                                                                <input type="text" value={Lastname} onChange={(e) => SetLastname(e.target.value)} className="form-control" name="lname" placeholder="Last Name*" id="lname" required />
                                                                <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                                                <span id="error_lastname"></span>
                                                            </div>
                                                            <div className="form-group  col-md-12">
                                                                <input type="email" value={Email} onChange={(e) => SetEmail(e.target.value)} className="form-control" placeholder="Email*" name="email" id="email" required />
                                                                <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/email.svg" alt="email" /></div>
                                                            </div>

                                                            <div className="form-group  col-md-12">
                                                                <input type="number" maxLength="11" onInput={maxLengthCheck} className="form-control" placeholder="Phone" name="phone" id="phone" />
                                                                <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/phone.svg" alt="phone" /></div>
                                                            </div>

                                                            <div className="form-group  col-md-12">
                                                                <textarea className="form-control" onChange={(e) => SetComments(e.target.value)} value={Comments} name="comment" rows="3" placeholder="Message*" required ></textarea>
                                                                <div className="form_icon comment_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/pi-chat.svg" alt="Message" /></div>
                                                            </div>
                                                            <div className="text-center col-md-12 m-auto align-items-center">
                                                                <button type="submit" className="btn blue_btn">SEND</button>
                                                            </div></div></div>}
                                                {loading && <div>Loading...</div>}
                                                {error && <div className="error_msg text-center"><p className="text_black font_bold text_18 text-center">Something went wrong. Please try again later.</p></div>}
                                                {success && <div className="error_msg text-center"><h1 className="head_h2 font_24 font_bold ">Thank you! </h1><p className="text-center text_black font_bold text_18">We will be in touch with you shortly.</p></div>}
                                            </div>
                                        );
                                    }}
                                </ZapierForm>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <TrustpilotReviews></TrustpilotReviews>

        </>
    );
    return (
        <>
            <Head>
                <title>Contact Us | Contact The Batch Leads Support Team| Batch Leads</title>
                <meta name="description" content="Do you have wholesale related questions for us and how to use the platform? Our team is happy to help you with answers." />
                <meta name="keywords" content="wholesale business" />
                <link rel="canonical" href="https://batchleads.io/contact-us/" />
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
            </Head>
            <DefaultLayout View={PageMarkup} />
        </>
    );
}

export default Contact;