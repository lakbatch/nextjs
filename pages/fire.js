import React from 'react';
import Link from "next/link";
import Head from "next/head";

import { RegisterButtonAffiliate } from "../Components//GlobalComponents/CTAButons";
import AffiRight from '../Components/GlobalComponents/AffiliateRight';
import AffiliateLayout from '../Components/LayoutComponents/AffiliateLayout';

// import Logo from "../../../Assets/Images/logo.svg";
// import FireImg from '../../../Assets/Images/fire.svg';

const Fire = () => {
    const AffiliateFPID = "fire";

    return (
        <>
            <Head>
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
            </Head>
            <main className="main_content">
                <section className="header_top pb-0">
                    <div className="container text-center position-relative">
                        <Link className="logo_top" href="/"><img src="./Assets/Images/logo.svg" alt="logo" /></Link>
                    </div>
                    <div className="banner_sec my-5 pt-0">
                        <div className="container">
                            <div className="row  swap_coloum justify-content-center">
                                <div className="col-md-6 mt-3 aos-init aos-animate" data-aos="fade-right" data-aos-duration="1200">
                                    <img src="./Assets/Images/fire.svg" alt="hero" />
                                </div>
                                <div className="col-md-6 trail-right">
                                    <h2 className="head_h2 black_text m-0 text_upr font_bebas font_50 font_reg">5,000 PROPERTY RECORDS FOR</h2>
                                    <h1 className="head_h1 trail-h1 blue_text  my-4 text_upr font_xbold mt-0">FREE</h1>
                                    <div className="col-md-12 cloud-right add_text aos-init aos-animate" data-aos="fade-left" data-aos-duration="1200">

                                        <AffiRight> </AffiRight>
                                        <RegisterButtonAffiliate FPID={AffiliateFPID} /></div>
                                </div>

                            </div>




                        </div>
                    </div>

                </section>
                <AffiliateLayout FPID={AffiliateFPID} />
            </main>

        </>
    )
}

export default Fire;