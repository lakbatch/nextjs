import { React } from "react";

const PageHeading = (props) => {
    return (
        <>
            <section className="inner_bnr d-flex align-items-center">
                <div className="container text-center w-100">
                    <h1 className="head_h1 font_bebas blue_text" data-aos="fade-up" data-aos-duration="1200">{props.text}</h1>
                </div>
            </section>
        </>
    )
}

export default PageHeading;