import React from "react";
import Link from "next/link";
import Head from "next/head";

import Footer from "../Components/LayoutComponents/Footer";
import GetStarted from "../Components/GlobalComponents/GetStarted";

// import Logo from "../../../Assets/Images/logo.svg";
// import LandingImg from "../../../Assets/Images/landing.png";
// import Illustration from "../../../Assets/Images/illustration.png";
// import Illus from "../../../Assets/Images/illus.png";
// import IImg from "../../../Assets/Images/i-1.png";
// import IImg1 from "../../../Assets/Images/i-2.png";
// import IImg2 from "../../../Assets/Images/i-3.png";
// import IImg3 from "../../../Assets/Images/i-4.png";
// import IImg4 from "../../../Assets/Images/i-5.png";
// import Sass from "../../../Assets/Images/sass_features1-1-2.png";
// import DotedImg from "../../../Assets/Images/bg_underline.png";
// import BoderWhite from "../../../Assets/Images/dividerline.png";
// import Duplicatore from "../../../Assets/Images/003-duplicate.svg";
// import Motivated from "../../../Assets/Images/004-motivation.svg";
// import AddressV from "../../../Assets/Images/002-address.svg";
// import ListSKr from "../../../Assets/Images/005-checklist.svg";
// import FilterImg from "../../../Assets/Images/filter.svg";
// import Bgy from "../../../Assets/Images/bg_y.png";
// import Bgyn from "../../../Assets/Images/bg_nm.png";
// import BgObj from "../../../Assets/Images/obj.png";
// import Bgn from "../../../Assets/Images/bg_n.png";
// import Jamil2Img from "../../../Assets/Images/jamil2.png";

const JamilNew = () => {
  const AffiliateFPID = "jamil2";

  return (
    <>
    <Head>
        <title>500 Text Messages for $1 | Jamil | BatchLeads</title>
        <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
        <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
      </Head>
      <main className="main_content">
        <section className="headers_top pt-2 position-relative">
          <div className="container text-left position-relative">
            <Link className="logo_top" href="/">
              <img src="./Assets/Images/logo.svg" alt="logo" />
            </Link>
          </div>
          <div className="banner_sec mt-5 position-relative">
            <div className="container">
              <div className="row ">
                <div
                  className="col-md-6 aos-init aos-animate"
                  data-aos="fade-right"
                  data-aos-duration="1200"
                >
                  <div className="banner_text">
                    <h2 className="head_h2 font_42 black_text m-0 text_upr font_bold">
                      <i>500 Text Messages for</i>
                    </h2>
                    <h1 className="head_h1 trail-h1 blue_text font_bebas my-5 text_upr font_bold mt-0">
                      <i>Just $1</i>
                    </h1>
                    <p className="font_26 font_reg bbn">
                      <i>
                        Yes, you read that right – send 500 fully compliant
                        text-messages for just{" "}
                        <span className="blue_text">$1</span>. Act now because
                        this offer won’t last!
                      </i>
                    </p>
                  </div>
                </div>
                <div
                  className="col-md-6 text-right demo_video aos-init aos-animate"
                  data-aos="fade-left"
                  data-aos-duration="1200"
                >
                  <div
                    className="position-relative text-left hide_circle"
                    style={{ width: "auto" }}
                  >
                    <img
                      className="extra_bg_herojamil"
                      src="./Assets/Images/landing.png"
                      alt="hero"
                    />
                  </div>
                  <img className="jamil2-hero" src="./Assets/Images/jamil2.png" alt="Ryan" />
                </div>
              </div>
              <div className="jamil-btn">
                <GetStarted FPID={AffiliateFPID}></GetStarted>
              </div>
            </div>
          </div>
        </section>
        <section className="tec_sec landing-d more-d pb-5">
          <div className="container text-center">
            <h2 className="head_h2  black_text font_bold font_40">
              Text Messaging Is the Best Way to Reach Leads
            </h2>
            <div className="row align-items-center">
              <div
                className="col-md-6 aos-init aos-animate"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <img src="./Assets/Images/illustration.png" alt="illustration" />
              </div>

              <div className="col-md-6 pt-5">
                <div className="source_text text-left">
                  <h4 className="lt_greynew font_xbold font_bebas pb-3 font_30">
                    Did you know…
                  </h4>
                  <div className="blackDot_img"></div>
                  <ul className="data_listhand list-unstyled">
                    <li>90% of text messages are read within 3 seconds.</li>
                    <li>98% of text messages are read by the recipient.</li>
                    <li>
                      35% of response rates are delivered from text messages.
                    </li>
                  </ul>
                  <p className="orange_text mt-5 font_25">
                    <i>Those numbers are incredible!</i>
                  </p>
                </div>
              </div>
            </div>
            <div className="text-center justify-aligns-center">
              <p className="lt_grey font_bold mt-3 text-center font_28">
                And now, for a limited time, we’re offering you 500 text
                messages for{" "}
                <span className="font_36  blue_text font_bold">just $1</span>
              </p>
              <GetStarted FPID={AffiliateFPID}></GetStarted>
            </div>
          </div>
        </section>

        <img src="./Assets/Images/bg_n.png" alt="bg" className="divider4"></img>

        <section className="source_landing trial-source">
          <div className="container">
            <h2 className="data_head font_bebas  blue_text font_xbold font_40 text-center txt_upr">
              500 Text Messages for Just $1!
            </h2>
            <p className="text_black text-center font_26 font_med lt_grey ">
              SMS messaging is the most effective channel for real estate
              investors in today’s market. It’s out-performing everything –
              email, direct mail, cold-calling, and everything else..
            </p>
            <p className="lt_grey font_bold my-5 text-center font_28 orange_arr">
              Now, we’re offering you the opportunity to test our system out
              with 500 messages for just $1!
            </p>

            <div className="row pt-5 align-items-top">
              <div
                className="col-md-6 pt-5 aos-init aos-animate"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <img src="./Assets/Images/illus.png" alt="illustration" />
              </div>

              <div className="col-md-6">
                <div className="source_text text-left">
                  <h4 className="lt_greynew font_xbold font_bebas font_30">
                    Our platform offers:
                  </h4>
                  <div className="blackDot_img"></div>
                  <ul className="data_listcheck list-unstyled">
                    <li>Full Compliance</li>
                    <li>Property Details with Zestimate</li>
                    <li>Vacant Property Identification</li>
                    <li>Best in Class Deliverability</li>
                    <li>100% Free Litigator Scrub</li>
                    <li>List Stacking</li>
                    <li>Local Phone Numbers & Call Forwarding</li>
                    <li>Rotating Message Templates</li>
                    <li>Instant Skip Tracing</li>
                  </ul>
                </div>
              </div>
            </div>

            <p className="text_black text-center font_26 font_med lt_grey pt-5 ">
              Simply put – we’ve built the best real estate SMS platform that
              exists<br></br> on the market today!
            </p>
            <p className="lt_grey font_bold pt-5 text-center font_25">
              And this is your chance to get started for{" "}
              <span className="font_36  blue_text font_bold">just $1</span>
            </p>

            <GetStarted FPID={AffiliateFPID}></GetStarted>
          </div>
        </section>

        <img src="./Assets/Images/obj.png" alt="bg" className="divider3"></img>

        <section className="dark_blue_landing align-items-center text-center py-5">
          <div className="container ">
            <h2 className="blue_arr data_head font_bebas  font_xbold font_42 text-center txt_upr">
              This Sounds Too Good to Be True
            </h2>
            <p className="text_black text-center font_26 font_reg">
              Maybe you’re wondering why we’d give you 500 SMS messages for only
              $1?
            </p>
            <img src="../Assets/Images/dividerline.png" alt="border" className="arrowline" />
            <p className="landing_head  font_med mt-3 text-center font_30">
              <span className="font_bold">Here’s the truth:</span> we’re making
              this offer to you because we want you to try our system.
            </p>
            <p className="landing_head  font_med pt-5 text-center font_34 orange_text">
              We think you’ll love it, but if you don’t, no problem – you’re not
              committing to anything!
            </p>
            <h2 className="landing_head font_bebas pt-5 font_xbold font_42 text-center txt_upr">
              Here’s how it works:
            </h2>
            <img src="../Assets/Images/dividerline.png" alt="border" className="arrowline" />
            <p className="text_black text-center font_34 font_xbold pt-4 lt_grey">
              Today, you can start a 14-day trial of our Ryan system<br></br>{" "}
              for just $1.
            </p>
            <img src="./Assets/Images/bg_underline.png" alt="border" className="arrowline" />

            <div className="row landing_list py-5 m-auto text-items-center">
              <div className="col-md-2">
                <img src="./Assets/Images/i-1.png" alt="ill" />
                <p>500 SMS Messages</p>
              </div>
              <div className="col-md-2">
                <img src="./Assets/Images/i-2.png" alt="ill" />
                <p>5 Phone Numbers</p>
              </div>
              <div className="col-md-2">
                <img src="./Assets/Images/i-3.png" alt="ill" />
                <p>Litigator Scrub</p>
              </div>
              <div className="col-md-2">
                <img src="./Assets/Images/i-4.png" alt="ill" />
                <p>Landline Removal</p>
              </div>
              <div className="col-md-2">
                <img src="./Assets/Images/i-5.png" alt="ill" />
                <p>And So Much More!</p>
              </div>
            </div>

            <h2 className="data_head white_text pt-4 font_med font_26 text-center">
              And, you can cancel your trial at any time, automatically and
              without contacting our support team!
            </h2>
            <p className=" font_med mt-3 text-center font_30">
              Don’t miss this opportunity{" "}
              <span className="font_36 font_xbold orange_text">just $1</span>
            </p>

            <GetStarted FPID={AffiliateFPID}></GetStarted>
          </div>
        </section>
        <img src="./Assets/Images/bg_nm.png" alt="bg" className="divider2"></img>

        <section className="leads py-5 text-align-center">
          <div className="container">
            <h2 className="landing_head font_bebas font_xbold font_42 text-center txt_upr blue_text">
              Text Messaging Is the Best Way to Reach Leads
            </h2>
            <div className="text-center py-5">
              <h2 className="blue_arr data_head m-auto  font_bebas font_xbold font_30 text-center txt_upr black_text">
                Not convinced?
              </h2>
            </div>

            <div className="row pt-5 align-items-top">
              <div
                className="col-md-7 pt-5 aos-init aos-animate"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <div className="what_is_wrapper">
                  <div className="what_is_sections">
                    <div className="row_1">
                      <div className="single_sec">
                        <img src="./Assets/Images/002-address.svg" className="tophero" alt="address" />
                        <p className="sec_title">Address Validation</p>
                        <p className="sec_desc">
                          Validate address very quickly
                        </p>
                      </div>
                      <div className="single_sec">
                        <img src="./Assets/Images/005-checklist.svg" alt="list" />
                        <p className="sec_title">Unlimited List Layering</p>
                        <p className="sec_desc">Get unlimited list layering</p>
                      </div>
                      <div className="single_sec">
                        <img src="./Assets/Images/004-motivation.svg" alt="motivate" />
                        <p className="sec_title">Highly Motivated</p>
                        <p className="sec_desc">
                          Get highly motivated leads easily
                        </p>
                      </div>
                    </div>
                    <div className="row_2">
                      <div className="single_sec">
                        <img src="./Assets/Images/filter.svg" alt="filter" />
                        <p className="sec_title">Filter Vacant Properties</p>
                        <p className="sec_desc">Get vacant properties data</p>
                      </div>
                      <div className="single_sec">
                        <img src="./Assets/Images/003-duplicate.svg" alt="duplicate" />
                        <p className="sec_title">Duplicate Removal</p>
                        <p className="sec_desc">Remove duplicate leads</p>
                      </div>
                    </div>
                  </div>
                  <img src="./Assets/Images/sass_features1-1-2.png" className="bg_what_is" alt="Hero" />
                </div>
              </div>

              <div className="col-md-5">
                <div className="source_text text-left">
                  <h4 className="lt_greynew font_xbold font_bebas font_30 here_new">
                    Here are some stats that might change your mind…
                  </h4>
                  <div className="blackDot_img"></div>
                  <ul className="data_listcheck list-unstyled text-black">
                    <li>
                      The average American checks their phone 47 times a day.
                    </li>
                    <li>Property Details with Zestimate</li>
                    <li>
                      SMS messages have a 209% higher response rate than phone,
                      email, or Facebook.
                    </li>
                    <li>SMS messages have a 98% open rate.</li>
                    <li>90% of SMS messages are read within 3 minutes.</li>
                    <li>
                      The average person responds to an SMS message within 90
                      seconds.
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>

        <img src="./Assets/Images/bg_y.png" alt="bg" className="divider"></img>
        <section className="subs">
          <div className="container">
            <div className="text-center">
              <h2 className="data_head font_bebas  black_text font_xbold font_40 text-center txt_upr blue_arr">
                What Our Users Say
              </h2>
            </div>
            <div className="row">
              <div className="col-md-6">
                <iframe
                  width="560"
                  height="315"
                  title="author-video"
                  src="https://www.youtube.com/embed/NsvkawT9-N4"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
              </div>

              <div className="col-md-6">
                <iframe
                  width="560"
                  height="315"
                  title="author-video"
                  src="https://www.youtube.com/embed/CIzwlhNMuxI?autoplay=0"
                  frameBorder="0"
                  allowFullScreen="1"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                ></iframe>
              </div>

              <div className="col-md-6">
                <iframe
                  width="560"
                  height="315"
                  title="author-video"
                  src="https://www.youtube.com/embed/RWdWjtTm0Lg?autoplay=0"
                  frameBorder="0"
                  allowFullScreen="1"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                ></iframe>
              </div>

              <div className="col-md-6">
                <iframe
                  width="560"
                  height="315"
                  title="author-video"
                  src="https://www.youtube.com/embed/cHTdazR7O1g?autoplay=0"
                  frameBorder="0"
                  allowFullScreen="1"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                ></iframe>
              </div>
            </div>
            <GetStarted FPID={AffiliateFPID}></GetStarted>
          </div>
        </section>
        <Footer></Footer>
      </main>
    </>
  );
};

export default JamilNew;
