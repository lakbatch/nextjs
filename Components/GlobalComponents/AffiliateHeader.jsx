import React from "react";



const AffiHeader = () => {

    return (
        <>


            <div className="banner_text trail_textp">
                <h2 className="head_h2 black_text m-0 text_upr font_bebas font_50 font_reg">5,000 PROPERTY RECORDS FOR</h2>
                <h1 className="head_h1 trail-h1 blue_text  my-4 text_upr font_xbold mt-0">FREE</h1>
                <p className="font_20 ">Yes, you read that right - create your own custom Real Estate list for FREE.<span>Act now because this offer won’t last!</span></p>
            </div>

        </>
    )
}

export default AffiHeader;
