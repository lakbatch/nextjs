import React from "react";
// import Duplicatore from "../../Assets/Images/003-duplicate.svg";
// import Motivated from "../../Assets/Images/004-motivation.svg";
// import AddressV from "../../Assets/Images/002-address.svg";
// import ListSKr from "../../Assets/Images/005-checklist.svg";
// import CenterMac from "../../Assets/Images/center-mac.svg";
// import FilterImg from "../../Assets/Images/filter.svg";

const CallToAction = () => {
  return (
    <>

      <div className="outer_box position-relative col-md-12">
        <div className="row pt-3 align-items-center text-center">

          <div className="col-md-3 first_box aos-animate" data-aos="fade-right" data-aos-duration="1200">
            <div className="home_box col-md-12">
              <img src="./Assets/Images/filter.svg" alt="filter" />
              <h6 className="head_h6 blue_text font_bold">Filter Vacant Properties</h6>
              <p>Identify which properties are flagged as vacants by USPS
</p>
            </div>
            <div className="home_box bottm_box col-md-12 aos-animate" data-aos="fade-right" data-aos-duration="1200">
              <img src="./Assets/Images/003-duplicate.svg" alt="duplicate" />
              <h6 className="head_h6 blue_text font_bold">Duplicate Removal</h6>
              <p>Removes all duplicate data instantaneously


</p>

            </div></div>

          <div className="side_box center_mac col-md-6">
            <div className=" mac_mid col-md-12"><img src="./Assets/Images/center-mac.svg" alt="increase" /></div>

            <div className="aos-animate" data-aos="fade-up" data-aos-duration="1200">
              <div className="home_box mid_box col-md-6 m-auto">
                <img src="./Assets/Images/004-motivation.svg" alt="motivate" />
                <h6 className="head_h6 blue_text font_bold">Highly Motivated</h6>
                <p>Easily find the MOST<br></br>motivated sellers
</p>
              </div>
            </div>
          </div>

          <div className="col-md-3 last_box aos-animate" data-aos="fade-left" data-aos-duration="1200">
            <div className="home_box col-md-12">
              <img src="./Assets/Images/002-address.svg" alt="increase" />
              <h6 className="head_h6 blue_text font_bold">Address Validation</h6>
              <p>All addresses are USPS validated before being uploaded
</p>
            </div>
            <div className="home_box bottm_box col-md-12 aos-animate" data-aos="fade-left" data-aos-duration="1200">
              <img src="./Assets/Images/005-checklist.svg" alt="list" />
              <h6 className="head_h6 blue_text font_bold">Unlimited List Stacking</h6>
              <p>Unlimited list stacking<br></br> and filtering
</p>

            </div></div>


        </div>

      </div>



    </>
  )
}

export default CallToAction;