import React, { useEffect, createContext, useState } from "react";
import TagManager from 'react-gtm-module';
import AOS from 'aos';
import { useRouter } from 'next/router'

import { AppWrapper, useAppContext } from "../context"
import "aos/dist/aos.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "../styles/globals.css";
import "../styles/Responsive.css";
import "../styles/App.css";


// const AppContext = createContext();

function MyApp({ Component, pageProps }) {

  // const [UrlParams, SetUrlParams] = useState(localStorage.getItem("BD_PARAMS"));

  // const { UrlParams, SetUrlParams } = useAppContext();

  useEffect(() => {
    AOS.init();
    // let QueryParams = window.location.search;
    // if (QueryParams !== "") {
    //   // SetUrlParams(QueryParams);
    //   console.log(useAppContext())
    //   localStorage.setItem("BD_PARAMS", QueryParams);
    // } else {
    //   // SetUrlParams("");
    // }
  }, []);

  function _ScrollToTop(props) {
    const { pathname } = useRouter();
    useEffect(() => {
      window.scrollTo({
        top:0, 
        left:0,
        behavior: 'smooth'
      });
      const tagManagerArgs = {
        gtmId: 'GTM-NL2H8NM',
        dataLayer: {
          event: "page",
          page: pathname
        }
      }

      TagManager.initialize(tagManagerArgs);
    }, [pathname]);
    return props.children;
  }

  // const ScrollToTop = withRouter(_ScrollToTop);
  const ScrollToTop = _ScrollToTop;

  return <ScrollToTop> <AppWrapper><Component {...pageProps} /> </AppWrapper></ScrollToTop>
}

export default MyApp
