import { React } from "react";
import Link from 'next/link'
// import InstaLogo from "../../Assets/Images/bx_bxl-instagram-alt.svg";
// import FbLogo from "../../Assets/Images/entypo-social_facebook-with-circle.svg";
// import MacImg from "../../Assets/Images/MacOS.svg";
// import WindowImg from "../../Assets/Images/Windows.svg";
// import LinImg from "../../Assets/Images/Linux.svg";
// import YTubeLogo from "../../Assets/Images/ph_youtube-logo-fill.svg";


const Footer = () => {
    return (
        <>
            <footer className="footer_sec">
                <div className="container text-center">
                    <h4 className="blue_text">All Platforms Supported</h4>
                    <div className="min_footer ">
                        <div className="row pt-2 pb-2  data_head justify-content-between align-items-center">

                            <div className="col-md-4">
                                <Link href={{ pathname: "https://apps.apple.com/us/app/batchleads/id1507452245" }} target="_blank" title="Mac"><img src="./Assets/Images/MacOS.svg" alt="Macos" /></Link>
                            </div>

                            <div className="col-md-4">
                                <Link href={{ pathname: "https://play.google.com/store/apps/details?id=com.batchleadstacker" }} target="_blank" title="Windows"><img src="./Assets/Images/Windows.svg" alt="Window" /></Link>
                            </div>
                            <div className="col-md-4">
                                <Link href={{ pathname: "https://app.batchleads.io/public/register" }} target="_blank" title="Linux"><img src="./Assets/Images/Linux.svg" alt="Linux" /></Link>
                            </div>



                        </div></div>

                    <p>@BatchLeads 2021. All rights reserved.</p>
                    <div className="footer_menu">
                        <Link href="/terms" title="">Terms of Service</Link>
                        <Link href="/privacy-policy" title="">Privacy Policy</Link>
                        <Link href="/personal-information" title="">Personal Information</Link>
                    </div>
                    <div className="social_icon">
                        <Link href={{ pathname: "https://www.facebook.com/groups/BatchService" }} target="_blank" title=""><img src="./Assets/Images/entypo-social_facebook-with-circle.svg" alt="facebook" target="_blank" className="facicon" /></Link>
                        <Link href={{ pathname: "https://www.instagram.com/batch_leads/" }} target="_blank" title=""><img src="./Assets/Images/bx_bxl-instagram-alt.svg" alt="Insta" target="_blank" /></Link>
                        <Link href={{ pathname: "https://www.youtube.com/c/BatchTV" }} target="_blank" title=""><img src="./Assets/Images/ph_youtube-logo-fill.svg" alt="Insta" target="_blank" /></Link>

                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer;