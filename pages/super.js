import React from 'react';
import Head from 'next/head';
import Link from 'next/link';

import { RegisterButtonAffiliate } from "../Components/GlobalComponents/CTAButons";
import AffiliateLayout from '../Components/LayoutComponents/AffiliateLayout';

// import Logo from "../../../Assets/Images/logo.svg";
// import SuperImg from "../../../Assets/Images/supar.png"
// import SuperHuman from "../../../Assets/Images/SuperHuman.png"


const Super = () => {
    const AffiliateFPID = "super";

    return (
        <>
            <Head>
                <title>5000 Free Property Records SUPER | BatchLeads</title>
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
            </Head>
            <main className="main_content">
                <section className="header_top pb-0">
                    <div className="container text-center position-relative">
                        <Link className="logo_top" href="/"><img src="./Assets/Images/logo.svg" alt="logo" /></Link>
                    </div>
                    <div className="banner_sec mt-5">
                        <div className="container">
                            <div className="row align-item-center justify-content-center">
                                <div className="col-md-6 aos-init aos-animate" data-aos="fade-right" data-aos-duration="1200">
                                    <div className="banner_text trail_textp">
                                        <h2 className="head_h2 black_text m-0 text_upr font_bebas font_50 font_reg">5,000 PROPERTY RECORDS FOR</h2>
                                        <h1 className="head_h1 trail-h1 blue_text  my-4 text_upr font_xbold mt-0">FREE</h1></div></div>
                                <div className="col-md-6 text-right demo_video aos-init aos-animate" data-aos="fade-left" data-aos-duration="1200">
                                    <div className="super_hero">
                                        <img className="extra_bg_hero" src="./Assets/Images/supar.png" alt="hero" />
                                    </div>

                                </div>
                                <div className="super-bottom cont_mini  col-md-12">
                                    <div className="row">
                                        <div className="col-md-5 col-sm-6 super-left">
                                            <div className="cloud-affi">
                                                <p className="font_20 ">Yes, you read that right - create your own custom Real Estate list for FREE.
                                    <span className="d-block pt-3 font_bold">Act now because this offer won’t last!</span></p>
                                                <RegisterButtonAffiliate FPID={AffiliateFPID} />
                                            </div>
                                        </div>
                                        <div className=" col-md-5 col-sm-6">
                                            <img src="./Assets/Images/SuperHuman.png" alt="human" />

                                        </div>



                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
                <AffiliateLayout FPID={AffiliateFPID} />
            </main>
        </>
    )
}

export default Super;