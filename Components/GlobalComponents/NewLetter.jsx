import React, { useState } from "react";
// import NewsImg from "../../Assets/Images/news.svg";


const NewsLetter = () => {
	const [Email, SetEmail] = useState("");

	return (
		<>
			<section className="signupnew">
				<div className="container">
					<div className="row">
						<div className="newlwtterimg col-md-2 col-4">
							<img src="./Assets/Images/news.svg" alt="img" className="lazyloaded" />

						</div>
						<div className="heading  col-md-4 col-sm-6 col-8">
							<h2>NEWSLETTER</h2>
							<p>Subscribe to our weekly investment updates and stay tuned!</p></div>
						<div className="inline-form col-md-6 pt-5">
							<form className="form-inline form_box">
								<div className="row">
									<div className="form-group col-md-9 col-8 mb-2">

										<input type="text" readonly className="form-control" id="staticEmail2" placeholder="Enter Your Email" value={Email} onChange={(e) => SetEmail(e.target.value)} />
									</div>

									<div className="sub_btn col-md-3  col-4 mb-2"><button type="submit" className="btn orange_btn " id="newsLet">Subscribe</button></div></div>
							</form>


						</div>

					</div></div>
			</section>



		</>
	)
}

export default NewsLetter;