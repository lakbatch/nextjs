import React, { useState, useEffect } from 'react';
import Link from 'next/link'
import { useRouter } from 'next/router'

import Footer from "./Footer";
// import Logo from "../../Assets/Images/logo.svg";
import { Nav, Navbar } from 'react-bootstrap';





const DefaultLayout = (props) => {
    const [ShowFixedHeader, SetShowFixedHeader] = useState(false);
    const [hashedLocation, setHashedLocation] = useState('');

    const router = useRouter()

    const ScrollHandle = () => {
        let ScrollPos = window.scrollY;
        if (ScrollPos >= 80) {
            SetShowFixedHeader(true);
        }
        else {
            SetShowFixedHeader(false);
        }
    }

    const getUrlParam = (sParam) => {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    useEffect(() => {
        setHashedLocation(window.location.hash)
    }, [])

    useEffect(() => {
        let xx = window.location.search;
        if (xx !== "") {
            let ButtonsArray = document.querySelectorAll('a[href*="app.batchleads"]');
            ButtonsArray.forEach((Button) => {

                let __url = Button.getAttribute('href');
                let __symbol = (() => {
                    return (__url.indexOf("?") === -1 ? "?" : "&")
                })();
                let __newUrl = (() => {
                    let __tempUrl = __url + __symbol + (xx.substring(1, xx.length));

                    return (xx.indexOf("deal=") !== -1 && __url.indexOf("_fp_ref_id") === -1 ? __tempUrl + "&_fp_ref_id=" + getUrlParam("deal") : __tempUrl);
                })();
                Button.setAttribute("href", __newUrl);
            });
        }
        if (window.location.hash !== "") {
            let TargetHash = window.location.hash;
            let TargetDiv = TargetHash.slice(1);
            if (document.getElementById(TargetDiv) !== null) {
                let MenuFeat = document.getElementById(TargetDiv).offsetTop;
                setTimeout(() => {
                    window.scrollTo({
                        top: MenuFeat - 75,
                        behavior: 'smooth',
                    })
                }, 500);
            }
        }
        window.addEventListener('scroll', ScrollHandle);
        return () => {
            window.removeEventListener('scroll', ScrollHandle);
        };


    }, []);

    const MenuScroll = (Id) => {
        if (document.getElementById(Id) !== null) {
            let MenuFeat = document.getElementById(Id).offsetTop;
            window.scrollTo({
                top: MenuFeat - 75,
                behavior: 'smooth',
            })
        }

    }
    const MenuScrollTop = (className) => {
        if (document.getElementsByClassName(className) !== null) {
            let MenuFeat = document.getElementsByClassName(className).offsetTop;
            window.scrollTo({
                top: MenuFeat - 0,
                behavior: 'smooth',
            })
        }

    }




    return (
        <>
            <header className={ShowFixedHeader ? "header darkHeader" : "header"}>
                <Navbar bg="light" expand="lg">
                    <div className="container custom_head">
                        <Link href="/" passHref><Navbar.Brand href="/"><img src="./Assets/Images/logo.svg" alt="logo" /></Navbar.Brand></Link>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto d-flex align-items-center">
                                <Link href="/" passHref>
                                    <Nav.Link className={`nav-link ${router.pathname === "/" && hashedLocation == "" ? 'active' : ''}`} onClick={() => MenuScrollTop('home')}>
                                        <i className="fa fa-home" aria-hidden="true"></i>
                                    </Nav.Link>
                                </Link>
                                <Link href="/#features" passHref>
                                    <Nav.Link className={`nav-link ${hashedLocation == "#features" ? 'active' : ''}`} onClick={() => MenuScroll('features')} >
                                        Features
                                   </Nav.Link>
                                </Link>

                                <Link href="/pricing" passHref>
                                    <Nav.Link className={`nav-link ${router.pathname === "/pricing" ? 'active' : ''}`} >
                                        Pricing
                                    </Nav.Link>
                                </Link>

                                <Nav.Link href="https://blog.batchleads.io/" className="nav-link" >Blog</Nav.Link>

                                <Link href="/faq" passHref>
                                    <Nav.Link className={`nav-link ${router.pathname
                                        === "/faq" ? 'active' : ''}`}>
                                        FAQ
                                    </Nav.Link>
                                </Link>
                                <Link href="/contact-us" passHref>
                                    <Nav.Link className={`nav-link ${router.pathname
                                        === "/contact-us" ? 'active' : ''}`}>
                                        Contact Us
                                    </Nav.Link>
                                </Link>
                                <Link href="//app.batchleads.io/public/login" passHref>
                                    <Nav.Link className="btn blue_btn" >Login</Nav.Link>
                                </Link>
                                <Link href="//app.batchleads.io/public/register" passHref>
                                    <Nav.Link className="btn">Sign Up </Nav.Link>
                                </Link>
                            </Nav>

                        </Navbar.Collapse>

                    </div>
                </Navbar>
            </header>


            <main className="main_content pt_78">
                {props.View}
            </main>
            <Footer />


        </>
    )
}

export default DefaultLayout;