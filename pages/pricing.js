import React from 'react';
import Head from 'next/head'
import ReactTooltip from "react-tooltip";
import { TabContent } from "react-bootstrap";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tabs';

import PageHeading from '../Components/GlobalComponents/PageHeading';
// import MarkQ from "../../Assets/Images/qmark.svg";
// import Month1 from "../../Assets/Images/mlist.svg";
// import Month2 from "../../Assets/Images/mlist1.svg";
// import Month3 from "../../Assets/Images/mlist2.svg";
// import MonthSms from "../../Assets/Images/sms1.svg";
// import MonthSms1 from "../../Assets/Images/sms2.svg";
// import MonthSms2 from "../../Assets/Images/sms3.svg";
// import Year1 from "../../Assets/Images/year1.svg";
// import Year2 from "../../Assets/Images/year2.svg";
// import Year3 from "../../Assets/Images/year3.svg";
// import Year4 from "../../Assets/Images/year4.svg";
// import Year5 from "../../Assets/Images/year5.svg";
// import Year6 from "../../Assets/Images/year6.svg";
import DefaultLayout from '../Components/LayoutComponents/DefaultLayout';
import Testimonialslider from '../Components/GlobalComponents/TestimonialSlider';


const Pricing = () => {


    const PageMarkup = (
        <>
            <PageHeading text="Pricing" />

            <section className="pricing_sec">
                <div className="container text-center">

                    <div className="price_box position-relative">

                        <div className="price_tabs">

                            <Tabs defaultActiveKey="month" transition={false} id="month-tabs" className="tabx_bx mnth_bx">
                                <Tab eventKey="month" title="Monthly" className="tab_bx ">

                                    <TabContent>
                                        <div className="price_cntnt active">
                                            <div className="row gap_0">
                                                <div className="col-md-4">
                                                    <div className="price_dtl" data-aos="fade-right" data-aos-duration="1200">
                                                        <h3 className="white_text text_upr position-relative m-0">BASE
                                            <p className="white_text font_15">*Complimentary Onboarding</p>
                                                        </h3>
                                                        <div className="price_list">
                                                            <h2 className="blue_text m-0"> $39 <span className="black_text">/mo</span></h2>
                                                            <small>Paid Monthly</small>
                                                            <ul className="list-unstyled text-left">
                                                                <li> <span> 100,000</span> Contacts <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>$0.14</span> Skip Trace per Match <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>1</span> User <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>5</span> Custom Fields <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                            </ul>
                                                            <div className="all_feature">
                                                                <span className="view_box">
                                                                    <button type="button" className="view_all" data-tip="div" data-for="registerBase">View All Features</button>
                                                                    <ReactTooltip id="registerBase" className="all_ftxt" place="right" effect="" >
                                                                        <div className="all_ftxtt">
                                                                            <ul className="check_batch list-unstyled text-left">
                                                                                <li>Vacant Property Identification &amp; Filtering</li>
                                                                                <li>Monthly Vacancy Checks &amp; Verification</li>
                                                                                <li>Unlimited List Stacking &amp; Filtering</li>
                                                                                <li>Address Correction, Validation &amp; Standardization (CASS)</li>
                                                                                <li>Ultra-Fast Skip Tracing</li>
                                                                                <li>Unlimited Lists &amp; Tags</li>
                                                                                <li>Lightning Fast Processing Speeds</li>
                                                                                <li>Absentee Owner Filtering</li>
                                                                                <li>Live Chat &amp; Email Support</li>
                                                                                <li>Facebook Support Group</li>
                                                                                <li>Monthly Training Calls</li>
                                                                                <li>Zapier Support</li>
                                                                            </ul>
                                                                        </div>
                                                                    </ReactTooltip>
                                                                </span>
                                                            </div>
                                                            <a className="btn blue_btn" href="https://app.batchLeads.io/public/register"> CHOOSE PLAN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="price_dtl active">
                                                        <h3 className="white_text text_upr position-relative m-0">PERFORMANCE
                                            <p className="white_text font_15">*Complimentary Onboarding</p>

                                                        </h3>
                                                        <div className="price_list">
                                                            <h2 className="blue_text m-0"> $129 <span className="black_text">/mo</span></h2>
                                                            <small>Paid Monthly</small>

                                                            <ul className="list-unstyled text-left">
                                                                <li> <span> 500,000</span> Contacts <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>$0.12</span> Skip Trace per Match <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>2-4 </span> Users <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>10</span> Custom Fields <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                            </ul>
                                                            <div className="all_feature">
                                                                <span className="view_box">
                                                                    <button type="button" className="view_all" data-tip="div" data-for="registerTip">View All Features</button>
                                                                    <ReactTooltip id="registerTip" className="all_ftxt" place="right" effect="solid" >
                                                                        <div className="all_ftxtt">
                                                                            <ul className="check_batch list-unstyled text-left">
                                                                                <li>Vacant Property Identification &amp; Filtering</li>
                                                                                <li>Monthly Vacancy Checks &amp; Verification</li>
                                                                                <li>Unlimited List Stacking &amp; Filtering</li>
                                                                                <li>Address Correction, Validation &amp; Standardization (CASS)</li>
                                                                                <li>Ultra-Fast Skip Tracing</li>
                                                                                <li>Unlimited Lists &amp; Tags</li>
                                                                                <li>Lightning Fast Processing Speeds</li>
                                                                                <li>Absentee Owner Filtering</li>
                                                                                <li>Live Chat &amp; Email Support</li>
                                                                                <li>Facebook Support Group</li>
                                                                                <li>Monthly Training Calls</li>
                                                                                <li>Zapier Support</li>
                                                                            </ul>
                                                                        </div>
                                                                    </ReactTooltip>
                                                                </span>
                                                            </div>
                                                            <a className="btn blue_btn" href="https://app.batchLeads.io/public/register"> CHOOSE PLAN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="price_dtl" data-aos="fade-left" data-aos-duration="1200">
                                                        <h3 className="white_text text_upr position-relative m-0">PERFORMANCE+
                                            <p className="white_text font_15">*Complimentary Onboarding</p>
                                                        </h3>
                                                        <div className="price_list">
                                                            <h2 className="blue_text m-0"> $299 <span className="black_text">/mo</span></h2>
                                                            <small>Paid Monthly</small>
                                                            <ul className="list-unstyled text-left">
                                                                <li> <span> 2.000,000</span> Contacts <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>$0.10</span> Skip Trace per Match <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>Unlimited </span> Users <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>15 </span> Custom Fields <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                            </ul>
                                                            <div className="all_feature">
                                                                <span className="view_box">
                                                                    <button type="button" className="view_all" data-tip="div" data-for="registerPlus">View All Features</button>
                                                                    <ReactTooltip id="registerPlus" className="all_ftxt" place="right" effect="solid" >
                                                                        <div className="all_ftxtt">
                                                                            <ul className="check_batch list-unstyled text-left">
                                                                                <li>Vacant Property Identification &amp; Filtering</li>
                                                                                <li>Monthly Vacancy Checks &amp; Verification</li>
                                                                                <li>Unlimited List Stacking &amp; Filtering</li>
                                                                                <li>Address Correction, Validation &amp; Standardization (CASS)</li>
                                                                                <li>Ultra-Fast Skip Tracing</li>
                                                                                <li>Unlimited Lists &amp; Tags</li>
                                                                                <li>Lightning Fast Processing Speeds</li>
                                                                                <li>Absentee Owner Filtering</li>
                                                                                <li>Live Chat &amp; Email Support</li>
                                                                                <li>Facebook Support Group</li>
                                                                                <li>Monthly Training Calls</li>
                                                                                <li>Zapier Support</li>
                                                                            </ul>
                                                                        </div>
                                                                    </ReactTooltip>
                                                                </span>
                                                            </div>
                                                            <a className="btn blue_btn" href="https://app.batchLeads.io/public/register"> CHOOSE PLAN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="testimonial_head lists_y" data-aos="fade-up" data-aos-duration="1200">
                                                <h3 className="head_h3 blue_text m-0">MIX & MATCH YOUR ADD-ONS (optional)</h3>
                                                <p>Add-ons <span className="blue_text">ONLY</span> available with a subscription from above </p>
                                            </div>

                                            <div className="row1 mb-3" data-aos="fade-up" data-aos-duration="1200">
                                                <h4> Lists </h4>
                                                <span> <img src="./Assets/Images/mlist.svg" alt="list1" /> </span>
                                                <span> <img src="./Assets/Images/mlist1.svg" alt="list2" /> </span>
                                                <span> <img src="./Assets/Images/mlist2.svg" alt="list3" /> </span>
                                            </div>
                                            <div className="row1 mb-3" data-aos="fade-up" data-aos-duration="1200">
                                                <h4> SMS </h4>
                                                <span> <img src="./Assets/Images/sms1.svg" alt="mlist1" /> </span>
                                                <span> <img src="./Assets/Images/sms2.svg" alt="mlist2" /> </span>
                                                <span> <img src="./Assets/Images/sms3.svg" alt="mlist3" /> </span>
                                            </div>

                                        </div>
                                    </TabContent>
                                </Tab>


                                <Tab eventKey="yearly" title="Yearly" className="tab_bx">
                                    <TabContent>
                                        <div className="price_cntnt ">
                                            <div className="row gap_0">
                                                <div className="col-md-4">
                                                    <div className="price_dtl" data-aos="fade-right" data-aos-duration="1200">
                                                        <h3 className="white_text text_upr position-relative m-0">BASE
                                        <p className="white_text font_15">*Complimentary Onboarding</p>
                                                        </h3>
                                                        <div className="price_list">
                                                            <h2 className="blue_text m-0"> $30 <span className="black_text">/mo</span></h2>
                                                            <small>Paid Yearly</small>
                                                            <ul className="list-unstyled text-left">
                                                                <li> <span> 100,000</span> Contacts <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>$0.14</span> Skip Trace per Match <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>1</span> User <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>5</span> Custom Fields <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                            </ul>
                                                            <div className="all_feature">
                                                                <span className="view_box">
                                                                    <button type="button" className="view_all " data-tip="div" data-for="registerBaseYear">View All Features</button>
                                                                    <ReactTooltip id="registerBaseYear" className="all_ftxt" place="right" effect="solid" >
                                                                        <div className="all_ftxtt">
                                                                            <ul className="check_batch list-unstyled text-left">
                                                                                <li>Vacant Property Identification &amp; Filtering</li>
                                                                                <li>Monthly Vacancy Checks &amp; Verification</li>
                                                                                <li>Unlimited List Stacking &amp; Filtering</li>
                                                                                <li>Address Correction, Validation &amp; Standardization (CASS)</li>
                                                                                <li>Ultra-Fast Skip Tracing</li>
                                                                                <li>Unlimited Lists &amp; Tags</li>
                                                                                <li>Lightning Fast Processing Speeds</li>
                                                                                <li>Absentee Owner Filtering</li>
                                                                                <li>Live Chat &amp; Email Support</li>
                                                                                <li>Facebook Support Group</li>
                                                                                <li>Monthly Training Calls</li>
                                                                                <li>Zapier Support</li>
                                                                            </ul>
                                                                        </div>
                                                                    </ReactTooltip>
                                                                </span>
                                                            </div>
                                                            <a className="btn blue_btn" href="https://app.batchLeads.io/public/register"> CHOOSE PLAN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="price_dtl active">
                                                        <h3 className="white_text text_upr position-relative m-0">PERFORMANCE
                                        <p className="white_text font_15">*Complimentary Onboarding</p>

                                                        </h3>
                                                        <div className="price_list">
                                                            <h2 className="blue_text m-0"> $100 <span className="black_text">/mo</span></h2>
                                                            <small>Paid Yearly</small>

                                                            <ul className="list-unstyled text-left">
                                                                <li> <span> 500,000</span> Contacts <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>$0.12</span> Skip Trace per Match <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>2-4 </span> Users <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>10</span> Custom Fields <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                            </ul>
                                                            <div className="all_feature">
                                                                <span className="view_box">
                                                                    <button type="button" className="view_all " data-tip="div" data-for="registerPlusYear">View All Features</button>
                                                                    <ReactTooltip id="registerPlusYear" className="all_ftxt" place="right" effect="" >
                                                                        <div className="all_ftxtt">
                                                                            <ul className="check_batch list-unstyled text-left">
                                                                                <li>Vacant Property Identification &amp; Filtering</li>
                                                                                <li>Monthly Vacancy Checks &amp; Verification</li>
                                                                                <li>Unlimited List Stacking &amp; Filtering</li>
                                                                                <li>Address Correction, Validation &amp; Standardization (CASS)</li>
                                                                                <li>Ultra-Fast Skip Tracing</li>
                                                                                <li>Unlimited Lists &amp; Tags</li>
                                                                                <li>Lightning Fast Processing Speeds</li>
                                                                                <li>Absentee Owner Filtering</li>
                                                                                <li>Live Chat &amp; Email Support</li>
                                                                                <li>Facebook Support Group</li>
                                                                                <li>Monthly Training Calls</li>
                                                                                <li>Zapier Support</li>
                                                                            </ul>
                                                                        </div>
                                                                    </ReactTooltip>
                                                                </span>
                                                            </div>
                                                            <a className="btn blue_btn" href="https://app.batchLeads.io/public/register"> CHOOSE PLAN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="price_dtl" data-aos="fade-left" data-aos-duration="1200">
                                                        <h3 className="white_text text_upr position-relative m-0">PERFORMANCE+
                                                        <p className="white_text font_15">*Complimentary Onboarding</p>
                                                        </h3>
                                                        <div className="price_list">
                                                            <h2 className="blue_text m-0"><span className="discount_txt"> $299 </span> $233 <span className="black_text">/mo</span></h2>
                                                            <small>Paid Yearly</small>
                                                            <ul className="list-unstyled text-left">
                                                                <li> <span> 2.000,000</span> Contacts <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>$0.10</span> Skip Trace per Match <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>Unlimited </span> Users <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                                <li> <span>15 </span> Custom Fields <img src="./Assets/Images/qmark.svg" alt="mark" /></li>
                                                            </ul>
                                                            <div className="all_feature">
                                                                <span className="view_box">
                                                                    <button type="button" className="view_all" data-tip="div" data-for="registerTipMonth">View All Features</button>
                                                                    <ReactTooltip id="registerTipMonth" className="all_ftxt" place="right" effect="" >
                                                                        <div className="all_ftxtt">
                                                                            <ul className="check_batch list-unstyled text-left">
                                                                                <li>Vacant Property Identification &amp; Filtering</li>
                                                                                <li>Monthly Vacancy Checks &amp; Verification</li>
                                                                                <li>Unlimited List Stacking &amp; Filtering</li>
                                                                                <li>Address Correction, Validation &amp; Standardization (CASS)</li>
                                                                                <li>Ultra-Fast Skip Tracing</li>
                                                                                <li>Unlimited Lists &amp; Tags</li>
                                                                                <li>Lightning Fast Processing Speeds</li>
                                                                                <li>Absentee Owner Filtering</li>
                                                                                <li>Live Chat &amp; Email Support</li>
                                                                                <li>Facebook Support Group</li>
                                                                                <li>Monthly Training Calls</li>
                                                                                <li>Zapier Support</li>
                                                                            </ul>
                                                                        </div>
                                                                    </ReactTooltip>
                                                                </span>
                                                            </div>
                                                            <a className="btn blue_btn" href="https://app.batchLeads.io/public/register"> CHOOSE PLAN</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="testimonial_head lists_y" data-aos="fade-up" data-aos-duration="1200">
                                                <h3 className="head_h3 white_text m-0">MIX & MATCH YOUR ADD-ONS (optional)</h3>
                                                <p>Add-ons <span className="blue_text">ONLY</span> available with a subscription from above </p>
                                            </div>

                                            <div className="row1 mb-3" data-aos="fade-up" data-aos-duration="1200">
                                                <h4> Lists </h4>
                                                <span> <img src="./Assets/Images/year1.svg" alt="Ylist" /> </span>
                                                <span> <img src="./Assets/Images/year2.svg" alt="Ylist1" /> </span>
                                                <span> <img src="./Assets/Images/year3.svg" alt="Ylist2" /> </span>
                                            </div>
                                            <div className="row1 mb-3" data-aos="fade-up" data-aos-duration="1200">
                                                <h4> SMS </h4>
                                                <span> <img src="./Assets/Images/year4.svg" alt="Ysms" /> </span>
                                                <span> <img src="./Assets/Images/year5.svg" alt="Ysms2" /> </span>
                                                <span> <img src="./Assets/Images/year6.svg" alt="Ysms3" /> </span>
                                            </div>

                                        </div>
                                    </TabContent>
                                </Tab>



                            </Tabs>


                        </div>
                    </div>

                </div>


            </section>

            <Testimonialslider />
        </>
    );

    return (
        <>
            <Head>
                <title>Batch Leads Pricing | Sign up for Free | Choose your Plan</title>
                <meta name="description" content="Batch Leads offers best in industry skip tracing for real estate agents, investors and wholesalers. Pricing plans designed to fit your skip tracing needs." />
                <meta name="keywords" content="Batch leads pricing" />
                <link rel="canonical" href="https://batchleads.io/pricing" />
                
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
    
            </Head>
            <DefaultLayout View={PageMarkup} />
        </>
    )
}

export default Pricing;