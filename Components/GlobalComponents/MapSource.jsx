import React from "react";
// import Layer1 from "../../Assets/Images/layer1.svg";
// import Layer2 from "../../Assets/Images/layer2.svg";
// import Layer3 from "../../Assets/Images/layer3.svg";
// import Layer4 from "../../Assets/Images/layer4.svg";
// import Layer5 from "../../Assets/Images/layer5.svg";
// import SourceBanner from "../../Assets/Images/B-center.svg";
// import BlackBorderLong from "../../Assets/Images/border-long.svg";

const MapSource = () => {
    return (
        <>
            <div className="row align-items-center mt-5">
                <div className="col-md-6 col-md-6" data-aos="fade-right" data-aos-duration="1200">
                    <div className="row source_img ">
                        <div className="b_logo col-md-5 col-5">
                            <img src="./Assets/Images/B-center.svg" alt="source-imgs" /></div>
                        <div className="source_layer col-md-7 col-7">

                            <img src="/Assets/Images/layer1.svg" alt="sourcelayer" className="layer1" />
                            <img src="./Assets/Images/layer2.svg" alt="sourcelayer1" className="layer2" />
                            <img src="./Assets/Images/layer3.svg" alt="sourcelayer2" className="layer3" />
                            <img src="./Assets/Images/layer4.svg" alt="sourcelayer3" className="layer4" />
                            <img src="./Assets/Images/layer5.svg" alt="sourcelayer4" className="layer5" />

                        </div>




                    </div>
                </div>
                <div className="col-md-6" data-aos="fade-left" data-aos-duration="1200">
                    <div className="source_text">
                        <h4 className="blue_text font_xbold font_30">Map Search <span className="black_text"> Key Features:</span></h4>
                        <div className="blackDot_img"><img src="./Assets/Images/B-center.svg" alt="border" /></div>
                        <ul className="data_list list-unstyled">
                            <li>Nationwide Property Search</li>
                            <li>Nationwide Buyers List</li>
                            <li>Comparable Home Sales</li>
                            <li>Quick Filters</li>
                            <li>100’s of Prospect Filters</li>
                            <li>And Much More!</li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}

export default MapSource;