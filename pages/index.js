import dynamic from "next/dynamic";
import IntegrationSliderDemo from "../Components/GlobalComponents/IntegrationSlider";
import Head from "next/head";



export default function Home() {
  const DefaultLayout = dynamic(
    () => import("../Components/LayoutComponents/DefaultLayout"),
    { loading: () => <div>Loading...</div> }
  );
  const CTAButons = dynamic(
    () => import("../Components/GlobalComponents/CTAButons"),
    { loading: () => <div>Loading...</div> }
  );
  const TrustpilotReviews = dynamic(
    () => import("../Components/GlobalComponents/TrustpilotReviews"),
    { loading: () => <div>Loading...</div> }
  );
  const Testimonialslider = dynamic(
    () => import("../Components/GlobalComponents/TestimonialSlider"),
    { loading: () => <div>Loading...</div> }
  );
  const HomeTabs = dynamic(
    () => import("../Components/GlobalComponents/HomeTabs"),
    { loading: () => <div>Loading...</div> }
  );
  const TecSection = dynamic(
    () => import("../Components/GlobalComponents/TecSection"),
    { loading: () => <div>Loading...</div> }
  );
  const MapSource = dynamic(
    () => import("../Components/GlobalComponents/MapSource"),
    { loading: () => <div>Loading...</div> }
  );
  const CallToAction = dynamic(
    () => import("../Components/GlobalComponents/CallToAction"),
    { loading: () => <div>Loading...</div> }
  );
  
  const PageMarkup = (
    <>
      <section className="banner_sec phone_dilr">
        <div className="container">
          <div className="row align-items-center">
            <div
              className="col-md-5 aos-init aos-animate"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <div className="banner_text price_dead">
                <h2 className="head_h2 black_text m-0 text_upr font_50 font_bebas">
                  ALL-IN-ONE PLATFORM
                </h2>
                <h1 className="head_h1 blue_text m-0 py-4 text_upr  ">
                  For Real Estate Professionals
                </h1>
                <CTAButons />
              </div>
            </div>
            <div
              className="banner_vid col-md-7 aos-init aos-animate"
              data-aos="fade-left"
              data-aos-duration="1200"
            >
              <div className="video_home">
                <video className="videoTag" autoPlay loop muted>
                  <source src="./Assets/Images/bl-new.mp4" type="video/mp4" />
                  <source src="./Assets/Images/bl-new.mp4" type="video/ogg" />
                </video>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="solar_div my-5">
        <div className="container">
          <IntegrationSliderDemo></IntegrationSliderDemo>
        </div>
      </section>
      <section className="data_sec" id="features">
        <div className="container">
          <h2 className=" blue_text font_xbold font_30 text-center ">
            All-In-One Platform for Real Estate Professionals
          </h2>
          <p className="text_black text-center font_15 pt-4">
            Finding Real Estate deals can be a challenge. But with BatchLeads,
            it doesn’t<br></br> have to be. We’ve created a one-stop solution to
            all your Real Estate needs… so that you can find more{" "}
            <b>
              <u>SELLERS</u>
            </b>
            , close more{" "}
            <b>
              <u>DEALS</u>
            </b>{" "}
            and maximize{" "}
            <b>
              <u>REVENUE</u>
            </b>{" "}
            !
          </p>
          <div className="data_box data_box_home_page mt-5">
            <div className="row">
              <HomeTabs />
            </div>
          </div>
        </div>
      </section>
      <section className="source_sec">
        <div className="container">
          <h2 className="data_head blue_text font_xbold font_30 text-center txt_upr">
            Nationwide Prospect Map Search
          </h2>
          <p className="text_black text-center font_18 lt_grey ">
            Finding prospects has never been this easy. At BatchLeads, we have
            made it simple and easy to access our nationwide list of <br></br>
            sellers and buyers.
          </p>

          <MapSource></MapSource>
        </div>
      </section>

      <section className="list_main  ">
        <div className="container">
          <h2 className="blue_text text-center font_xbold font_30 data_head  m-auto">
            <span className="black_text">List Manager </span>Gives You the Power
            to Find
            <br></br>Motivated Sellers Quickly
          </h2>
          <p className="text_black text-center font_18 pt-3">
            We help investors save time and money by pinpointing the leads in
            their lists that are most likely to sell.{" "}
          </p>
          <p className="text_black text-center font_18 pt-3">
            We help you{" "}
            <b>
              <u>SIMPLIFY</u>
            </b>
            ,{" "}
            <b>
              <u>MANAGE</u>
            </b>{" "}
            and{" "}
            <b>
              <u>ORGANIZE</u>
            </b>{" "}
            all your data in one place. We then help you “Stack” your lists and
            identify properties that appear on <br></br>multiple lists and have
            multiple distress indicators. These sellers are likely to be highly
            motivated and eager to sell.{" "}
          </p>
          <CallToAction />
        </div>
      </section>
      <div className="container">
        <div className="arrow_mail  text-center">
          <img
            className="mb-3"
            src="./Assets/Images/arrowborder.svg"
            alt="arrow"
          />
          <CTAButons></CTAButons>
        </div>
      </div>

      <section className="reach_sec">
        <div className="container">
          <div className="row align-items-center justify-content-between">
            <div className="know_box col-md-4  justify-content-between text-items-center">
              <h2 className="head_h2 font_bold text-center lt_grey ">
                Did you know…
              </h2>
              <div className=" row reach_list pt-4 d-flex align-items-center ">
                <div className="single_lead d-flex mb-4 ">
                  <div className="col-md-3">
                    <h2 className="blue_text font_xbold">90%</h2>
                  </div>
                  <div className="col-md-9">
                    <p className="head_h5 lt_grey">
                      {" "}
                      of text messages are read within 3 seconds.
                    </p>
                  </div>
                </div>

                <div className="single_lead  mb-4 d-flex ">
                  <div className="col-md-3">
                    <h2 className="blue_text font_xbold">98%</h2>
                  </div>
                  <div className="col-md-9">
                    <p className="head_h5 lt_grey">
                      {" "}
                      of text messages are read by the recipient.
                    </p>
                  </div>
                </div>

                <div className="single_lead d-flex ">
                  <div className="col-md-3">
                    <h2 className="blue_text font_xbold">35%</h2>
                  </div>
                  <div className="col-md-9">
                    <p className="head_h5 lt_grey">
                      {" "}
                      of response rates are delivered from text messages.
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div
              className="col-md-6 way_sec text-center aos-init"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <h3 className="lead_text font_med font_36 text_upr">
                Text Messaging Is the Best Way to Reach Leads
              </h3>
              <div className="d-block text-center mt-5">
                <img src="./Assets/Images/send-msg.svg" alt="send" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <h1 className="head_h2 blue_text text-center mt-4 font_30 font_bold">
        At <span className="black_text">Batch</span>Leads, We’ve Made{" "}
        <span className="black_text">Text Messaging</span> Easy!
      </h1>
      <section className="built_sec">
        <div className="container">
          <div className="row align-items-center">
            <div
              className="col-md-7 aos-init"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <img src="./Assets/Images/text-m.svg" alt="text" />
            </div>
            <div
              className="col-md-5"
              data-aos="fade-left"
              data-aos-duration="1200"
            >
              <div className="source_text">
                <h4 className="blue_text font_xbold font_30">
                  Text Messaging{" "}
                  <span className="black_text"> Key Features:</span>
                </h4>
                <div className="blackDot_img">
                  <img src="./Assets/Images/border-long.svg" alt="border" />
                </div>
                <ul className="data_list list-unstyled">
                  <li>Fully Compliant</li>
                  <li>Best in Class Deliverability</li>
                  <li>100% Free Litigator Scrub</li>
                  <li>Vacant Property Identification</li>
                  <li>Local Phone Numbers & Call Forwarding</li>
                  <li>Rotating Message Templates</li>
                  <li>Property Details with Zestimate</li>
                  <li>Easily Comp Properties</li>
                  <li>And Much More!</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="text-center mt-4">
            <CTAButons></CTAButons>
          </div>
        </div>
      </section>

      <section className="get_sec pb-5">
        <div className="container text-center">
          <h2 className="black_text font_bold font_30  m-0">Skip Tracing...</h2>
          <h2 className="blue_text font_xbold font_30 txt_upr">
            Faster, Easier and More Accurate Than Ever
          </h2>
          <p className="font_18 lt_grey data_box">
            Skip Tracing is the process of locating an individual's most recent
            contact information. Sometimes it includes multiple phone numbers,
            email addresses and even "
            <span className="orange_text font_bold">The Golden Address</span>".
            We give you a competitive advantage by only using tier-one data and
            you only pay for successful matches.
          </p>
          <div className="row align-items-center mt-5">
            <div
              className="col-md-6 aos-init"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <img src="./Assets/Images/skiptrac.svg" alt="text" />
            </div>
            <div
              className="col-md-6"
              data-aos="fade-left"
              data-aos-duration="1200"
            >
              <div className="source_text text-left">
                <h4 className="blue_text font_xbold font_30">
                  Skip Tracing{" "}
                  <span className="black_text"> Key Features:</span>
                </h4>
                <div className="blackDot_img">
                  <img src="./Assets/Images/border-long.svg" alt="border" />
                </div>
                <ul className="data_list list-unstyled">
                  <li>High Quality Tier-One Data</li>
                  <li> Only Pay for Successful Matches</li>
                  <li>Rapid Results</li>
                  <li>And Much More!</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="text-center my-3">
            <CTAButons />
          </div>
        </div>
      </section>
      <TrustpilotReviews />
      <section className="tec_sec">
        <div className="container text-center">
          <h2 className="head_h2  blue_text font_bold font_30">
            The Most Complete Property & Homeowner Database Nationwide
          </h2>

          <TecSection></TecSection>
        </div>
      </section>
      <section className="serve_sec">
        <div className="container">
          <div className="row cont_mini align-items-center">
            <div
              className="col-md-6"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <div className="media align-items-center serve_media">
                <div className="media-left d-flex align-items-center justify-content-center">
                  <img src="./Assets/Images/serve.svg" alt="serve" />
                </div>
                <div className="media-body">
                  <h5 className="head_h3 blue_text font_med">
                    Professional REI Platform
                  </h5>
                  <p className="m-0 black_text">
                    The most powerful and complete lead generation platform in
                    the industry. Locate sellers, buyers and lenders nationwide
                    in seconds.
                  </p>
                </div>
              </div>
            </div>
            <div
              className="col-md-6"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <div className="media align-items-center serve_media">
                <div className="media-left d-flex align-items-center justify-content-center">
                  <img src="./Assets/Images/serve2.svg" alt="serve" />
                </div>
                <div className="media-body">
                  <h5 className="head_h3 blue_text font_med">
                    Comparable Sales
                  </h5>
                  <p className="m-0 black_text">
                    Locate all nationwide active/pending/closed properties. All
                    50 states are updated daily with a fully detailed 5 year
                    listing history.
                  </p>
                </div>
              </div>
            </div>

            <div
              className="col-md-6"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <div className="media align-items-center serve_media">
                <div className="media-left d-flex align-items-center justify-content-center">
                  <img src="./Assets/Images/serve3.svg" alt="serve" />
                </div>
                <div className="media-body">
                  <h5 className="head_h3 blue_text font_med">
                    Find The Hottest Leads
                  </h5>
                  <p className="m-0 black_text">
                    Automatically generated quick-lists give you the hottest
                    leads nationwide in any market. Probates, pre-foreclosures,
                    inherited properties, cash buyers, tax defaults? It’s all
                    there.
                  </p>
                </div>
              </div>
            </div>
            <div
              className="col-md-6"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <div className="media align-items-center serve_media">
                <div className="media-left d-flex align-items-center justify-content-center">
                  <img src="./Assets/Images/serve4.svg" alt="serve" />
                </div>
                <div className="media-body">
                  <h5 className="head_h3 blue_text font_med">
                    Comparables All-In-One
                  </h5>
                  <p className="m-0 black_text">
                    Run comparables like the pros and make decisions on the spot
                    with simple but powerful comparable tool with built-in
                    nationwide active/pending/closed MLS data.
                  </p>
                </div>
              </div>
            </div>
          </div>

          <h3 className="black_text text-center font_med font_30 pt-3 my-3">
            Locate More Prospects, Talk to More People and Close More Deals With{" "}
            <span className="blue_text"> Less Work</span>!
          </h3>
          <div className="text-center m-auto pb-5">
            <CTAButons />
          </div>
        </div>
      </section>
.
      <Testimonialslider></Testimonialslider>
    </>
  );
  return (
    <>
      <Head>
        <title>The All-In-ONE Real Estate Data & Marketing Platform | Batch Leads Software</title>
        <meta name="description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue."/>
        <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large"/>
        <link rel="canonical" href="https://batchleads.io/"/>
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="The All-In-ONE Real Estate Data & Marketing Platform | Batch Leads Software" />
        <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
        <meta property="og:url" content="https://batchleads.io/" />
        <meta property="og:site_name" content="Batchleads.io" />
        <meta property="og:image" content="https://blog.batchleads.io/wp-content/uploads/2021/04/image-1.png" />
        <meta property="og:image:secure_url" content="https://blog.batchleads.io/wp-content/uploads/2021/04/image-1.png" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="627" />
        <meta property="og:image:alt" content="The All-In-ONE Real Estate Data & Marketing Platform | Batch Leads Software" />
        <meta property="og:image:type" content="image/png" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="The All-In-ONE Real Estate Data & Marketing Platform | Batch Leads Software" />
        <meta name="twitter:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
        <meta name="twitter:image" content="https://blog.batchleads.io/wp-content/uploads/2021/04/image-1.png" />
      </Head>
      <DefaultLayout View={PageMarkup} />
    </>
  );
}
