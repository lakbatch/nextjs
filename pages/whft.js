import React from "react";
import Link from "next/link";
import Head from "next/head";

import { RegisterButtonAffiliate } from "../Components/GlobalComponents/CTAButons";
import AffiRight from "../Components/GlobalComponents/AffiliateRight";
import AffiliateLayout from "../Components/LayoutComponents/AffiliateLayout";

// import Logo from "../../../Assets/Images/logo.svg";
// import MacImg from "../../../Assets/Images/single-desk.svg";
// import WhftImg from "../../../Assets/Images/whft.png";

const Whft = () => {
  const AffiliateFPID = "whft";

  return (
    <>
    <Head>
      <title>5000 Free Property Records WHFT | BatchLeads</title>
      <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
      <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
    </Head>
      <main className="main_content">
        <section className="header_top pb-0">
          <div className="container text-center position-relative">
            <Link className="logo_top" href="/">
              <img src="./Assets/Images/logo.svg" alt="logo" />
            </Link>
          </div>
          <div className="banner_sec mt-3">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-md-6 aos-init">
                  <div className="banner_text trail_textp">
                    <h2 className="head_h2 black_text m-0 text_upr font_bebas font_50 font_reg">
                      5,000 PROPERTY RECORDS FOR
                    </h2>
                    <h1 className="head_h1 trail-h1 blue_text  my-4 text_upr font_xbold mt-0">
                      FREE
                    </h1>
                  </div>
                  <div className="mt-5">
                    <img src="./Assets/Images/single-desk.svg" alt="red" />
                  </div>{" "}
                </div>
                <div className="col-md-6  cloud-down">
                  <img className="mb-3 whft-logo" src="./Assets/Images/whft.png" alt="Whft" />
                  <AffiRight> </AffiRight>
                  <RegisterButtonAffiliate FPID={AffiliateFPID} />
                </div>
              </div>
            </div>
          </div>
        </section>
        <AffiliateLayout FPID={AffiliateFPID} />
      </main>
    </>
  );
};

export default Whft;
