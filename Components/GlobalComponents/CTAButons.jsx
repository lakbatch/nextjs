import React, { useContext } from "react";
import { useAppContext } from "../../context";

const CTAButons = () => {
    return (
        <div className="demo_btns">
            <a className="btn" href="https://app.batchleads.io/public/register">GET STARTED TODAY</a>

        </div>
    )
}
const RegisterButtonAffiliate = (props) => {
    const { UrlParams } = useAppContext();
    return <a className="btn mt-3" href={`https://app.batchleads.io/public/register_leads_trial?_fp_ref_id=${props.FPID}${(UrlParams !== null && UrlParams ?  UrlParams.replace("?", "&") : "")}`}>START YOUR 7-DAY FREE TRIAL</a>
}

const RegisterLeadsButtonAffiliate = (props) => {
    const { UrlParams } = useAppContext();
    return <a className="btn mt-5" href={`https://app.batchleads.io/public/register_trial?_fp_ref_id=${props.FPID}${(UrlParams !== null ? (UrlParams).replace("?", "&") : "")}`}>START YOUR 7-DAY FREE TRIAL</a>
}

const RegisterButton = () => {
    return (
        <div className="py-4">
            <a className="btn orange_btn" href="https://app.batchleads.io/public/register_leads_trial">START YOUR 7-DAY FREE TRIAL</a></div>
    )
}
export default CTAButons;
export { RegisterButtonAffiliate, RegisterButton, RegisterLeadsButtonAffiliate };