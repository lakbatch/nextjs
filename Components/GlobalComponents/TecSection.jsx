import React from 'react';
// import CardImage1 from "../../Assets/Images/PAY.svg";
// import CardImage2 from "../../Assets/Images/ic2.svg";
// import CardImage3 from "../../Assets/Images/skip.svg";
// import CardImage4 from "../../Assets/Images/time.svg";
// import CardImage5 from "../../Assets/Images/property.svg";

const TecSection = () => {

    return (
        <>
            <div className="row justify-content-center pt-5">
                <div className="flex-tec">
                    <div className="tec_box">
                        <img src="./Assets/Images/PAY.svg" alt="host" />
                        <h2 className="blue_text font_bold font_40">3,100+</h2>
                        <p className="black_text font_med">Counties</p>
                    </div>
                </div>
                <div className="flex-tec">
                    <div className="tec_box">
                        <img src="./Assets/Images/ic2.svg" alt="dialer" />
                        <h2 className="blue_text font_bold font_40">155+</h2>
                        <p className="black_text font_med">Million U.S.<br></br> Property Parcels</p>
                    </div>
                </div>
                <div className="flex-tec">
                    <div className="tec_box secc_img">
                        <img src="./Assets/Images/skip.svg" alt="secure" />
                        <h2 className="blue_text font_bold font_40">99.8%</h2>
                        <p className="black_text font_med">U.S.<br></br> Population</p>
                    </div>
                </div>
                <div className="flex-tec">
                    <div className="tec_box secc_img">
                        <img src="./Assets/Images/time.svg" alt="secure" />
                        <h2 className="blue_text font_bold font_40">7</h2>
                        <p className="black_text font_med">Billion Recorded<br></br> Documents</p>
                    </div>
                </div>
                <div className="flex-tec">
                    <div className="tec_box secc_img">
                        <img src="./Assets/Images/property.svg" alt="secure" />
                        <h2 className="blue_text font_bold font_40">1.8</h2>
                        <p className="black_text font_med">Million Active<br></br> Listings</p>
                    </div>
                </div>

            </div>
        </>
    )
}

export default TecSection;