import React, { useState, useEffect, useRef } from 'react';
import { Modal } from 'react-bootstrap';
import Link from 'next/link'
// import UserIcon from "../../Assets/Images/user.svg";
// import EmailIcon from "../../Assets/Images/email.svg";
// import PhoneIcon from "../../Assets/Images/phone.svg";
// import CompanyIcon from "../../Assets/Images/company.svg";
// import GetDemoIcon from "../../Assets/Images/logo.svg";

import axios from 'axios';

const RequestDemoModal = () => {
    const [ShowModal, SetShowModal] = useState(false);
    const [Agents, SetAgents] = useState(1);
    const [Firstname, SetFirstname] = useState("");
    const [Lastname, SetLastname] = useState("");
    const [Email, SetEmail] = useState("");
    const [Phone, SetPhone] = useState("");
    const [Company, SetCompany] = useState("");
    const ModalRef = useRef(null);

    const SubmitForm = (e) => {
        e.preventDefault();
        const Data = {
            FirstName: Firstname,
            LastName: Lastname,
            Email: Email,
            Phone: Phone,
            Company: Company,
            Agents: Agents
        }
        axios({
            method: 'post',
            url: 'https://hooks.zapier.com/hooks/catch/2424086/o0bm884/',
            data: Data,
            headers: { 'Content-Type': 'multipart/form-data' }
        })
            .then(res => {
                const Status = res.status;
                if (Status === 200) {
                    SetShowModal(false);
                }
            });
    }

    useEffect(() => {
        SetFirstname("");
        SetLastname("");
        SetEmail("");
        SetPhone("");
        SetCompany("");
        SetAgents(1);
    }, [ShowModal]);

    return (
        <>
            <div className="demo_btns">
                <Link href="#" className="btn white_btn" onClick={() => SetShowModal(true)}>REQUEST Demo</Link>
            </div>
            <Modal ref={ModalRef} centered="true" show={ShowModal} className="custom_bodal request_modal" backdrop="static" keyboard={false} size="lg">
                <Modal.Body className="position-relative contact_body">
                    <button type="button" className="close position-absolute" onClick={() => SetShowModal(false)}>
                        <span aria-hidden="true">×</span>
                    </button>
                    <p>We Help Companies Do More With Less!</p>
                    <h2 className="head_h2 black_text font_bebas font_reg"><span className="blue_text">Get Your</span> Demo Today!</h2>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form_box text-center">
                                <form className="demoForm" onSubmit={(e) => SubmitForm(e)}>
                                    <div className="form-group">
                                        <input type="text" className="form-control" onChange={(e) => SetFirstname(e.target.value)} placeholder="First Name*" value={Firstname} id="fname" required />
                                        <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                        <span id="error_name"></span>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" onChange={(e) => SetLastname(e.target.value)} placeholder="Last Name*" value={Lastname} id="lname" required />
                                        <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                        <span id="error_lastname"></span>
                                    </div>
                                    <div className="form-group">
                                        <input type="email" className="form-control" onChange={(e) => SetEmail(e.target.value)} placeholder="Email*" value={Email} id="email" required />
                                        <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/email.svg" alt="email" /></div>
                                        <span id="error_email"></span>
                                    </div>
                                    <div className="form-group">
                                        <input type="number" className="form-control" onChange={(e) => SetPhone(e.target.value)} placeholder="Phone Number*" value={Phone} required id="number" />
                                        <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/phone.svg" alt="phone" /></div>
                                        <span id="error_tel"></span>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" onChange={(e) => SetCompany(e.target.value)} placeholder="Company Name" value={Company} id="company_name" />
                                        <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/company.svg" alt="company" /></div>

                                    </div>
                                    <div className="range_box form-group">
                                        <div className="range-slider range-value">
                                            <label> Number of Agents*</label>
                                            <input className="range-slider__range" id="new_range" type="range" value={Agents} min="1" max="10" onChange={(e) => SetAgents(e.target.value)} min="1" max="100" style={{ backgroundImage: `-webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${(Agents - 1) / 100}, rgb(54, 131, 188)), color-stop(${(Agents - 1) / 100}, rgb(170, 170, 170)))` }} />
                                            <span className="range-slider__value">{Agents}</span>
                                        </div>
                                        <span id="error_agent"></span>
                                    </div>
                                    <input type="text" value="Demo" placeholder="Trial Status" style={{ display: "none" }} />
                                    <div className="text-center" id="demo_submit">
                                        <input className="btn blue_bg w-100 " type="submit" value="REQUEST DEMO" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <img src="./Assets/Images/logo.svg" alt="demo" />
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )
}

export default RequestDemoModal;