import React, { useContext } from "react";
// import Secure from "../../Assets/Images/secure.png";
// import { AppContext } from "../../App";
import { useAppContext } from "../../context"

const GetStarted = (props) => {
    const { UrlParams } = useAppContext();
    return (
        <div className="securebtn text-center align-items-center justify-content-center mt-5">
            <a className="btn-square  my-3" href={`https://app.batchleads.io/public/register_trial?_fp_ref_id=${props.FPID}${(UrlParams && UrlParams !== null ? (UrlParams).replace("?", "&") : "")}`}>Get Started now</a>

            <div className="secure_btn col-md-12 text-center py-4">
                <img src="./Assets/Images/secure.png" alt="secure" className="text-center" /></div>
        </div>
    )
}

const GetStartedTrial = () => {
    return (
        <div className="securebtn text-center align-items-center justify-content-center mt-5">
            <a className="btn-square  my-3" href="https://app.batchleads.io/public/register_trial">Get Started now</a>

            <div className="secure_btn col-md-12 text-center py-4">
                <img src="./Assets/Images/secure.png" alt="secure" className="text-center" /></div>
        </div>
    )
}
export default GetStarted;
export { GetStartedTrial };
