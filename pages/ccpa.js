import React from "react";
import Head from 'next/head'
import ZapierForm from 'react-zapier-form'

import DefaultLayout from '../Components/LayoutComponents/DefaultLayout';
// import UserIcon from "../../Assets/Images/user.svg";
// import EmailIcon from "../../Assets/Images/email.svg";
// import CommentIcon from "../../Assets/Images/pi-chat.svg";
import TrustpilotReviews from "../Components/GlobalComponents/TrustpilotReviews";


const Ccpa = () => {


    const PageMarkup = (
        <>
            <section className="inner_bnr d-flex align-items-center">
                <div className="container text-center w-100">
                    <h1 className="head_h1 font_bebas blue_text font_reg" data-aos="fade-up" data-aos-duration="1200">California Consumer
 <span className="black_text d-block">Protection Act</span></h1>
                </div>
            </section>
            <section className="form_sec removal inner_dots">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-5 text_16 pp-p" data-aos="fade-right" data-aos-duration="1200">
                            <p className="black_text mb-0">To the extent required by the California Consumer Protection Act (CCPA): California residents who want to know what Personal Information (as defined in the CCPA) we maintain about them may send a Request to Know using the webform below or by calling us at <strong> (888) 351-4693 </strong> during business hours. We will confirm receipt of your Request within 10 days and fulfill your request within 45 days, or up to 90 days if we notify you as to why we need more time. You will first be required to prove your identity as required by the CCPA and as set forth in our Privacy Notice to California Residents.</p>
                            <p className="black_text">If we cannot successfully verify your identity, or if the information would be too sensitive to disclose, we may not be able to provide the Personal Information to you. You may only submit 2 requests per year.</p>
                        </div>

                        <div className="col-md-6 leftDot_line" data-aos="fade-left" data-aos-duration="1200">
                            <div className="form_box col-md-12 pt-4">
                                <ZapierForm action='https://hooks.zapier.com/hooks/catch/2424086/onm6ps4/'>
                                    {({ error, loading, success }) => {
                                        return (
                                            <div>
                                                {!success && !loading &&
                                                    <div>
                                                        <div className="form-group">
                                                            <input type="text" className="form-control" placeholder="First Name*" id="fname" name="fname" required />
                                                            <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                                            <span id="error_name"></span>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="text" className="form-control" placeholder="Last Name*" name="lname" id="lname" required />
                                                            <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/user.svg" alt="user" /></div>
                                                            <span id="error_lastname"></span>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="email" className="form-control" placeholder="Email*" name="email" id="email" required />
                                                            <div className="form_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/email.svg" alt="email" /></div>
                                                            <span id="error_email"></span>
                                                        </div>
                                                        <div className="form-group">
                                                            <textarea className="form-control" rows="3" name="comment" placeholder="Comment"></textarea>
                                                            <div className="form_icon comment_icon d-flex align-items-center justify-content-center"><img src="./Assets/Images/pi-chat.svg" alt="comment" /></div>
                                                        </div>
                                                        <div className="text-center">
                                                            <button className="btn blue_btn">SEND REQUEST</button>

                                                        </div>
                                                    </div>
                                                }
                                                {loading && <div>Loading...</div>}
                                                {error && <div className="error_msg text-center"><p className="text_black font_bold text_18 text-center">Something went wrong. Please try again later.</p></div>}
                                                {success && <div className="error_msg text-center"><h1 className="head_h2 font_24 font_bold ">Thank you! </h1><p className="text-center text_black font_bold text_18">We will be in touch with you shortly.</p></div>}
                                            </div>
                                        )
                                    }}
                                </ZapierForm>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <TrustpilotReviews></TrustpilotReviews>        </>
    );
    return (
        <>
            <Head>
                <title>California Consumer Protection Act | for California Users | BatchLeads</title>
                <meta name="description" content="The rights of all consumers in California are protected under the California Consumer Protection Act. Please read CCPA carefully before using BatchLeads services and submit a request here." />
                <meta name="keywords" content="California Consumer Protection Act, CCPA" />
                <link rel="canonical" href="https://batchleads.io/ccpa" />
                <meta property="title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
                <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
            </Head>
            <DefaultLayout View={PageMarkup} />
        </>
    )
}

export default Ccpa;