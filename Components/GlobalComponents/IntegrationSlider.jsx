import React from "react";
import Slider from "react-slick";

// import Studio from "../../Assets/Images/partner5.svg";
// import Investor from "../../Assets/Images/partner1.svg";
// import BKey from "../../Assets/Images/partner4.svg";
// import RealEstate from "../../Assets/Images/partner2.svg";
// import Titan from "../../Assets/Images/partner3.svg";
// import TTPImg from "../../Assets/Images/ttp.svg";
// import FutureLogo from "../../Assets/Images/FUTURE_FLIPPER.svg";


const IntegrationSliderDemo = () => {
    const Settings = {
        dots: false,
        infinite: 1,
        loop: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        rows: 1,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                },
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                },
            },
        ],
    };
    return (
        <section className=" logo_sec">
            <div className="container text-center">
                <h1 className="head_h2 font_30 text_upr font_bold white_text">Trusted By:</h1>
                <div className="logo_slider">
                    <div className="category pt-5">
                        <div className="col-md-12">
                            <div className="logo_slide">
                                <Slider {...Settings}>
                                    <div className="logo_items">

                                        <div className="logo_image"><img src="./Assets/Images/partner1.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">
                                        <div className="logo_image realimg"><img src="./Assets/Images/partner2.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">
                                        <div className="logo_image"><img src="./Assets/Images/partner3.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items bb-key">
                                        <div className="logo_image  "><img src="./Assets/Images/partner4.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">
                                        <div className="logo_image"><img src="./Assets/Images/partner5.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">

                                        <div className="logo_image ttpn"><img src="./Assets/Images/ttp.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">

                                        <div className="logo_image future"><img src="./Assets/Images/FUTURE_FLIPPER.svg" alt="logo" /></div>
                                    </div>

                                    <div className="logo_items">

                                        <div className="logo_image"><img src="./Assets/Images/partner1.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">
                                        <div className="logo_image realimg"><img src="./Assets/Images/partner2.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">
                                        <div className="logo_image"><img src="./Assets/Images/partner3.svg" alt="logo" /></div>
                                    </div>

                                    <div className="logo_items">
                                        <div className="logo_image"><img src="./Assets/Images/partner4.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">
                                        <div className="logo_image"><img src="./Assets/Images/partner5.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">

                                        <div className="logo_image ttpn"><img src="./Assets/Images/ttp.svg" alt="logo" /></div>
                                    </div>
                                    <div className="logo_items">

                                        <div className="logo_image future"><img src="./Assets/Images/FUTURE_FLIPPER.svg" alt="logo" /></div>
                                    </div>
                                </Slider>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    )
}

export default IntegrationSliderDemo;