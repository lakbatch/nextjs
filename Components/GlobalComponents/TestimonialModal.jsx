import React, { useState } from "react";
import { Modal } from 'react-bootstrap'
// import Videoicon from "../../Assets/Images/video.svg";

const TestimonialModal = (props) => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <button className="video_icon" data-toggle="modal" data-target={"#examplemodal"}><img src="./Assets/Images/video.svg" alt="User-img" onClick={handleShow} /></button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>

                </Modal.Header>
                <Modal.Body>{props.Video}</Modal.Body>

            </Modal>
        </>
    )
}

export default TestimonialModal;