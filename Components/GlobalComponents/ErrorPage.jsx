import React from 'react';
import DefaultLayout from '../LayoutComponents/DefaultLayout';
// import ErrorImage from '../../Assets/Images/error404.svg';

const ErrorPage = (props) => {
    const PageMarkup = (
        <>
            {props.Errortype === "Error404" ?
                <img src="./Assets/Images/error404.svg" className="error-img" alt="error" />
                : ""}
        </>
    );

    return <DefaultLayout View={PageMarkup} />
}

export default ErrorPage;