import React, { useEffect } from "react";
import Link from 'next/link'

const TrustpilotReviews = () => {
    const Ref = React.useRef(null);

    useEffect(() => {
        if (window.Trustpilot) {
            window.Trustpilot.loadFromElement(Ref.current, true);
        }
    }, []);

    return (
        <section className="testimonial_sec1">
            <div className="container">
                <div ref={Ref} className="trustpilot-widget" data-locale="en-US" data-template-id="54ad5defc6454f065c28af8b" data-businessunit-id="5fdc0238fcba90000135558d" data-style-height="240px" data-style-width="100%" data-theme="dark" data-stars="3,4,5" data-review-languages="en">
                    <Link href={{ pathname: "https://www.trustpilot.com/review/batchleads.io" }} target="_blank" rel="noopener">Trustpilot</Link>
                </div>
            </div>
        </section>


    )
}

export default TrustpilotReviews;