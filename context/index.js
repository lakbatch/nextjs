import { createContext, useContext, useState, useEffect } from 'react';

const AppContext = createContext();

export function AppWrapper({ children }) {

    const [UrlParams, SetUrlParams] = useState('');

    useEffect(() => {

        let QueryParams = window.location.search;
        if (QueryParams !== "") {
            SetUrlParams(QueryParams);
            localStorage.setItem("BD_PARAMS", QueryParams);
          } else {
            SetUrlParams("");
          }
    }, [])

    return (
        <AppContext.Provider value={UrlParams, SetUrlParams}>
            {children}
        </AppContext.Provider>
    );
}

export function useAppContext() {
    return useContext(AppContext);
}