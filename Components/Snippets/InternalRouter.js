import React from "react";
import { Switch, Route } from "react-router-dom";
import ErrorPage from '../GlobalComponents/ErrorPage';
import Homepage from '../RouteComponents/Homepage';
import PrivacyPolicy from '../RouteComponents/PrivacyPolicy';
import Terms from '../RouteComponents/Terms';
import PersonalInformation from '../RouteComponents/PersonalInformation';
import Ccpa from '../RouteComponents/Ccpa';
import Pricing from '../RouteComponents/Pricing';
import Trial from '../RouteComponents/Affiliates/Trial';
import Ceo from '../RouteComponents/Affiliates/Ceo';
import Ttp from '../RouteComponents/Affiliates/Ttp';
import Fire from '../RouteComponents/Affiliates/Fire';
import Zach from '../RouteComponents/Affiliates/Zach';
import Zack from '../RouteComponents/Affiliates/Zack';
import Pace from '../RouteComponents/Affiliates/Pace';
import Super from '../RouteComponents/Affiliates/Super';
import Jamil from '../RouteComponents/Affiliates/Jamil';
import WTM from '../RouteComponents/Affiliates/WTM-new';
import Wcw from '../RouteComponents/Affiliates/Wcw';
import Red from '../RouteComponents/Affiliates/Red';
import Julian from '../RouteComponents/Affiliates/Julian';
import DataKing from '../RouteComponents/Affiliates/DataKing';
import Titanium from '../RouteComponents/Affiliates/Titanium';
import Inu from '../RouteComponents/Affiliates/Inu';
import BPS from '../RouteComponents/Affiliates/BPS';
import JamilNew from '../RouteComponents/Affiliates/JamilNew';
import Price from '../RouteComponents/Affiliates/Price';
import Tiffany from '../RouteComponents/Affiliates/Tiffany';
import Reiaj from '../RouteComponents/Affiliates/Reiaj';
import Estes from '../RouteComponents/Affiliates/Estes';
import Homerun from '../RouteComponents/Affiliates/Homerun';
import WholeSalinginc from '../RouteComponents/Affiliates/WholeSalinginc';
import ShortSale from '../RouteComponents/Affiliates/ShortSale';
import Flipnerd from '../RouteComponents/Affiliates/Flipnerd';
import Virtual from '../RouteComponents/Affiliates/Virtual';
import Elevate from '../RouteComponents/Affiliates/Elevate';
import Tagg50 from '../RouteComponents/Affiliates/Tagg50';
import Flipping from '../RouteComponents/Affiliates/Flipping';
import Dave79 from '../RouteComponents/Affiliates/Dave79';
import Dave from '../RouteComponents/Affiliates/Dave';
import UnderGround from '../RouteComponents/Affiliates/UnderGround';
import Hustlefam from '../RouteComponents/Affiliates/Hustlefam';
import Squad from '../RouteComponents/Affiliates/Squade';
import Gavin from '../RouteComponents/Affiliates/Gavin';
import Club from '../RouteComponents/Affiliates/Club';
import Flipman from '../RouteComponents/Affiliates/Flipman';
import Alan from '../RouteComponents/Affiliates/Alan';
import Ryan from '../RouteComponents/Affiliates/Ryan';
import Cody from '../RouteComponents/Affiliates/Cody';
import Brandyn from '../RouteComponents/Affiliates/Brandyn';
import Whft from '../RouteComponents/Affiliates/Whft';
import Hbb from '../RouteComponents/Affiliates/Hbb';
import Closer from '../RouteComponents/Affiliates/Closer';
import BatchleadsTrial from '../RouteComponents/Affiliates/BatchleadsTrial';
import BlueNotes from '../RouteComponents/Affiliates/BlueNotes';
import Austin from '../RouteComponents/Affiliates/Austin';
import ShutUpnInvest from '../RouteComponents/Affiliates/ShutUpnInvest';
import Icon from '../RouteComponents/Affiliates/Icon';
import BatchTv from '../RouteComponents/Affiliates/BatchTv';
import TikTok from '../RouteComponents/Affiliates/TikTok';
import SquadUp from '../RouteComponents/Affiliates/Squadup';
import FlippingwithZack from '../RouteComponents/Affiliates/FlippingWithZack';
import Tab from '../RouteComponents/Affiliates/Tab';
import EFC from '../RouteComponents/Affiliates/EFC';
import Hbg from '../RouteComponents/Affiliates/Hbg';
import Tracy from '../RouteComponents/Affiliates/Tracy';
import Batch from '../RouteComponents/Affiliates/Batch';
import Anny from '../RouteComponents/Affiliates/Anny';
import Fasterfreedom from '../RouteComponents/Affiliates/Fasterfreedom';
import Burr from '../RouteComponents/Affiliates/Burr';
import Contact from "../RouteComponents/Contact";
import Faq from "../RouteComponents/Faq";
import Beyond from '../RouteComponents/Affiliates/Beyond';

function InternalRouter() {
  return (
    <>
      <Switch>
        <Route exact
          path="/"
          render={(props) => <Homepage {...props} />}
        ></Route>

        <Route exact
          path="/privacy-policy"
          render={(props) => <PrivacyPolicy {...props} />}
        ></Route>

        <Route exact
          path="/terms"
          render={(props) => <Terms {...props} />}
        ></Route>

        <Route exact
          path="/personal-information"
          render={(props) => <PersonalInformation {...props} />}
        ></Route>

        <Route exact
          path="/ccpa"
          render={(props) => <Ccpa {...props} />}
        ></Route>

        <Route exact
          path="/pricing"
          render={(props) => <Pricing {...props} />}
        ></Route>




        <Route exact
          path="/contact-us"
          render={(props) => <Contact {...props} />}
        ></Route>

        <Route exact
          path="/faq"
          render={(props) => <Faq {...props} />}
        ></Route>


        {/*********** Affiliate Routes ***********/}
        <Route exact
          path="/batchleads-trial-list"
          render={(props) => <BatchleadsTrial {...props} />}
        ></Route>
        <Route exact
          path="/trial"
          render={(props) => <Trial {...props} />}
        ></Route>

        <Route exact
          path="/ceo"
          render={(props) => <Ceo {...props} />}
        ></Route>

        <Route exact
          path="/ttp"
          render={(props) => <Ttp {...props} />}
        ></Route>
        <Route exact
          path="/fire"
          render={(props) => <Fire {...props} />}
        ></Route>

        <Route exact
          path="/zach"
          render={(props) => <Zach {...props} />}
        ></Route>
        <Route exact
          path="/zack"
          render={(props) => <Zack {...props} />}
        ></Route>

        <Route exact
          path="/pace"
          render={(props) => <Pace {...props} />}
        ></Route>

        <Route exact
          path="/wtm"
          render={(props) => <WTM {...props} />}
        ></Route>

        <Route exact
          path="/super"
          render={(props) => <Super {...props} />}
        ></Route>


        <Route exact
          path="/jamil"
          render={(props) => <Jamil {...props} />}
        ></Route>

        <Route exact
          path="/jamil2"
          render={(props) => <JamilNew {...props} />}
        ></Route>


        <Route exact
          path="/wcw"
          render={(props) => <Wcw {...props} />}
        ></Route>

        <Route exact
          path="/bluenotes"
          render={(props) => <BlueNotes {...props} />}
        ></Route>

        <Route exact
          path="/red"
          render={(props) => <Red {...props} />}
        ></Route>

        <Route path='/distruptor' 
        component={() => {
          window.location.href = '/red';
          return null;
        }} />


        <Route exact
          path="/austin"
          render={(props) => <Austin {...props} />}
        ></Route>

        <Route exact
          path="/tracy"
          render={(props) => <Tracy {...props} />}
        ></Route>



        <Route exact
          path="/burr"
          render={(props) => <Burr {...props} />}
        ></Route>

        <Route exact
          path="/icon"
          render={(props) => <Icon {...props} />}
        ></Route>

        <Route exact
          path="/shortsale"
          render={(props) => <ShortSale {...props} />}
        ></Route>

        <Route exact
          path="/shutupninvest"
          render={(props) => <ShutUpnInvest {...props} />}
        ></Route>

        <Route exact
          path="/julian"
          render={(props) => <Julian {...props} />}
        ></Route>

        <Route exact
          path="/dataking"
          render={(props) => <DataKing {...props} />}
        ></Route>



        <Route exact
          path="/wholesalinginc"
          render={(props) => <WholeSalinginc {...props} />}
        ></Route>


        <Route exact
          path="/titanium"
          render={(props) => <Titanium {...props} />}
        ></Route>


        <Route exact
          path="/reiaj"
          render={(props) => <Reiaj {...props} />}
        ></Route>

        <Route exact
          path="/tab"
          render={(props) => <Tab {...props} />}
        ></Route>

        <Route exact
          path="/estes"
          render={(props) => <Estes {...props} />}
        ></Route>

        <Route exact
          path="/flipnerd"
          render={(props) => <Flipnerd {...props} />}
        ></Route>


        <Route exact
          path="/virtual"
          render={(props) => <Virtual {...props} />}
        ></Route>

        <Route exact
          path="/hustlefam"
          render={(props) => <Hustlefam {...props} />}
        ></Route>

        <Route exact
          path="/underground"
          render={(props) => <UnderGround {...props} />}
        ></Route>

        <Route exact
          path="/elevate"
          render={(props) => <Elevate {...props} />}
        ></Route>

        <Route exact
          path="/tagg50"
          render={(props) => <Tagg50 {...props} />}
        ></Route>


        <Route exact
          path="/squad"
          render={(props) => <Squad {...props} />}
        ></Route>

        <Route exact
          path="/gavin"
          render={(props) => <Gavin {...props} />}
        ></Route>

        <Route exact
          path="/flipping"
          render={(props) => <Flipping {...props} />}
        ></Route>


        <Route exact
          path="/dave79"
          render={(props) => <Dave79 {...props} />}
        ></Route>

        <Route exact
          path="/dave"
          render={(props) => <Dave {...props} />}
        ></Route>
        <Route exact
          path="/bps"
          render={(props) => <BPS {...props} />}
        ></Route>

        <Route exact
          path="/brandyn"
          render={(props) => <Brandyn {...props} />}
        ></Route>

        <Route exact
          path="/inu"
          render={(props) => <Inu {...props} />}
        ></Route>

        <Route exact
          path="/tiffany"
          render={(props) => <Tiffany {...props} />}
        ></Route>

        <Route exact
          path="/price"
          render={(props) => <Price {...props} />}
        ></Route>

        <Route exact
          path="/whft"
          render={(props) => <Whft {...props} />}
        ></Route>

        <Route exact
          path="/batch"
          render={(props) => <Batch {...props} />}
        ></Route>
        <Route exact
          path="/homerun"
          render={(props) => <Homerun {...props} />}
        ></Route>

        <Route exact
          path="/club"
          render={(props) => <Club {...props} />}
        ></Route>

        <Route exact
          path="/cody"
          render={(props) => <Cody {...props} />}
        ></Route>
        <Route exact
          path="/flipman"
          render={(props) => <Flipman {...props} />}
        ></Route>

        <Route exact
          path="/ryan"
          render={(props) => <Ryan {...props} />}
        ></Route>
        <Route exact
          path="/hbb"
          render={(props) => <Hbb {...props} />}
        ></Route>

        <Route exact
          path="/hbg"
          render={(props) => <Hbg {...props} />}
        ></Route>
        <Route exact
          path="/closer"
          render={(props) => <Closer {...props} />}
        ></Route>
        <Route exact
          path="/batchtv"
          render={(props) => <BatchTv {...props} />}
        ></Route>

        <Route exact
          path="/tiktok"
          render={(props) => <TikTok {...props} />}
        ></Route>

        <Route exact
          path="/efc"
          render={(props) => <EFC {...props} />}
        ></Route>

        <Route exact
          path="/squadup"
          render={(props) => <SquadUp {...props} />}
        ></Route>


        <Route exact
          path="/flippingwithzack"
          render={(props) => <FlippingwithZack {...props} />}
        ></Route>

        <Route exact
          path="/alan"
          render={(props) => <Alan {...props} />}
        ></Route>

        <Route exact
          path="/anny"
          render={(props) => <Anny {...props} />}
        ></Route>

        <Route exact
          path="/beyond"
          render={(props) => <Beyond {...props} />}
        ></Route>


        <Route exact
          path="/fasterfreedom"
          render={(props) => <Fasterfreedom {...props} />}
        ></Route>

        {/******** If the page doesn;t exist, show a 404 page **********/}
        <Route render={() => <ErrorPage Errortype="Error404" />} />
        {/******** If the page doesn;t exist, show a 404 page **********/}
      </Switch>
    </>
  );
}

export default InternalRouter;