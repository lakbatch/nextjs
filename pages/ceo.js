import React from "react";
import Head from "next/head";
import Link from "next/link";

import { RegisterButtonAffiliate } from "../Components/GlobalComponents/CTAButons";
import AffiHeader from "../Components/GlobalComponents/AffiliateHeader";
import TecSection from "../Components/GlobalComponents/TecSection";
import MapSource from "../Components/GlobalComponents/MapSource";
import CallToAction from "../Components/GlobalComponents/CallToAction";
import Testimonialslider from "../Components/GlobalComponents/TestimonialSlider";
import Footer from "../Components/LayoutComponents/Footer";

// import Logo from "../../../Assets/Images/logo.svg";
// import serve2 from "../../../Assets/Images/serve2.svg";
// import serve4 from "../../../Assets/Images/serve4.svg";
// import MapComp from "../../../Assets/Images/compare-map.svg";
// import CeoImg from "../../../Assets/Images/ceo.png";
// import BlackBorder from "../../../Assets/Images/blck-border.svg";

const Ceo = () => {
  const AffiliateFPID = "ceo";

  return (
    <>
    <Head>
        <title>5000 Free Property Records CEO | BatchLeads</title>
        <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
        <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />
 
    </Head>

      <main className="main_content">
        <section className="header_top pb-0">
          <div className="container text-center position-relative">
            <Link className="logo_top" href="/">
              <img src="./Assets/Images/logo.svg" alt="logo" />
            </Link>
          </div>
          <div className="banner_sec mt-5">
            <div className="container">
              <div className="row justify-content-center">
                <div
                  className="col-md-6 aos-init aos-animate"
                  data-aos="fade-right"
                  data-aos-duration="1200"
                >
                  <AffiHeader></AffiHeader>
                  <RegisterButtonAffiliate FPID={AffiliateFPID} />
                </div>

                <div
                  className="col-md-6 text-right demo_video aos-init aos-animate"
                  data-aos="fade-left"
                  data-aos-duration="1200"
                >
                  <div className="hero ceo_hero" style={{ width: "auto" }}>
                    <img
                      className="extra_bg_hero"
                      src="./Assets/Images/ceo.png"
                      alt="hero"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="tec_sec less-mt trial-tec pt-0 pb-5">
          <div className="container text-center">
            <h2 className="head_h2  black_text font_reg font_40">
              The Most Complete Property & Homeowner Database Nationwide
            </h2>

            <TecSection></TecSection>
            <RegisterButtonAffiliate FPID={AffiliateFPID} />
          </div>
        </section>

        <div className="container">
          <h2 className="data_head blue_text font_xbold font_40 text-center txt_upr">
            Nationwide Prospect Map Search
          </h2>
          <p className="text_black text-center font_24 lt_grey ">
            Finding prospects has never been this easy. At BatchLeads, we have
            made it<br></br>simple and easy to access our nationwide list of
            sellers and buyers.
          </p>
        </div>
        <section className="source_sec trial-source mt-5">
          <div className="container">
            <MapSource></MapSource>
          </div>
        </section>

        <section className="serve_sec pt-5">
          <div className="container">
            <h2 className="text-center black_text font_xbold font_40">
              NOT CONVINCED?
            </h2>

            <div className="compare-head row align-items-center">
              <div
                className="col-md-5 col-sm-6 cloud-bg"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <h3 className="font_bold head_h2 ">
                  <span className="font_xbold blue_text d-block">
                    Compare Properties
                  </span>
                  With the Power of Nationwide MLS Access
                </h3>

                <RegisterButtonAffiliate FPID={AffiliateFPID} />
              </div>

              <div
                className="col-md-6 col-sm-5 mt-5"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <img src="./Assets/Images/compare-map.svg" alt="map-compare" />
              </div>
            </div>

            <div className="row  align-items-center">
              <div
                className="col-md-6"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <div className="media align-items-center serve_media mt-0">
                  <div className="media-left d-flex align-items-center justify-content-center">
                    <img src="./Assets/Images/serve2.svg" alt="serve" />
                  </div>
                  <div className="media-body">
                    <h5 className="head_h3 blue_text font_med">
                      Comparable Sales
                    </h5>
                    <p className="m-0 black_text">
                      Locate all nationwide active/pending/closed properties.
                      All 50 states are updated daily with a fully detailed 5
                      year listing history.
                    </p>
                  </div>
                </div>
              </div>

              <div
                className="col-md-6"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <div className="media align-items-center serve_media mt-0">
                  <div className="media-left d-flex align-items-center justify-content-center">
                    <img src="./Assets/Images/serve4.svg" alt="serve" />
                  </div>
                  <div className="media-body">
                    <h5 className="head_h3 blue_text font_med">
                      Comparables All-In-One
                    </h5>
                    <p className="m-0 black_text">
                      Run comparables like the pros and make decisions on the
                      spot with simple but powerful comparable tool with
                      built-in nationwide active/pending/closed MLS data.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="blackDot_img text-center pt-3">
          <img src="./Assets/Images/blck-border.svg" alt="border" />
        </div>

        <section className="list_main pt-0">
          <div className="container ">
            <h2 className="data_box blue_text text-center font_xbold text_upr font_40 m-auto pt-3">
              List Manager Gives You the Power to Find Motivated Sellers Quickly
            </h2>
            <p className="data_box text_black text-center font_24 pt-5">
              We help you <span className="blue_text font_bold">SIMPLIFY</span>,{" "}
              <span className="blue_text font_bold"> MANAGE </span>and{" "}
              <span className="blue_text font_bold">ORGANIZE</span> all your
              data in one place. We then help you “Stack” your lists and
              identify properties that appear on multiple lists and have
              multiple distress indicators. These sellers are likely to be
              highly motivated and eager to sell.{" "}
            </p>

            <CallToAction></CallToAction>
          </div>
          <div className="mt-5 text-center extra_space">
            <RegisterButtonAffiliate FPID={AffiliateFPID} />
          </div>
        </section>

        <Testimonialslider></Testimonialslider>
        <Footer></Footer>
      </main>
      
    </>
  );
};

export default Ceo;
