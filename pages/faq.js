import React, { useContext } from "react";
import { Accordion, Card } from "react-bootstrap";
import { useAccordionToggle } from "react-bootstrap/AccordionToggle";
import AccordionContext from "react-bootstrap/AccordionContext";
import Head from "next/head";

import DefaultLayout from "../Components/LayoutComponents/DefaultLayout";
import PageHeading from "../Components/GlobalComponents/PageHeading";
import TrustpilotReviews from "../Components/GlobalComponents/TrustpilotReviews";

function ContextAwareToggle({ children, eventKey, callback }) {
  const currentEventKey = useContext(AccordionContext);

  const decoratedOnClick = useAccordionToggle(
    eventKey,
    () => callback && callback(eventKey)
  );

  const isCurrentEventKey = currentEventKey === eventKey;

  return (
    <h5
      className={isCurrentEventKey ? "active" : "faq-ques"}
      onClick={decoratedOnClick}
    >
      {children}
    </h5>
  );
}

const Faq = () => {
  const PageMarkup = (
    <>
      <PageHeading text="FAQ" />
      <section className="faq_sec inner_dots">
        <div className="container text-center">
          <div className="row align-items-center">
            <div
              className="col-lg-7"
              data-aos="fade-right"
              data-aos-duration="1200"
            >
              <Accordion defaultActiveKey="0">
                <Card>
                  <ContextAwareToggle
                    as={Card.Header}
                    eventKey="0"
                    className="active"
                  >
                    Do I provide my own lists and/or can I purchase lists
                    through you?
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                      With the list package, you are able to pull a list
                      directly from your platform.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <ContextAwareToggle as={Card.Header} eventKey="1">
                    How accurate is your vacancy data?
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="1">
                    <Card.Body>
                      Our vacancy data is the most updated data available for
                      anyone. We are connected directly to the USPS servers and
                      vacancy checks occur monthly as USPS updates their
                      database.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <ContextAwareToggle as={Card.Header} eventKey="2">
                    How does your skip tracing compare to other skip tracing
                    companies?
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="2">
                    <Card.Body>
                      We only use tier-1 skip tracing data and provide the best
                      information for prospects currently available to the
                      industry including phone numbers and emails.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>

                <Card>
                  <ContextAwareToggle as={Card.Header} eventKey="3">
                    Do I pay for all data I submit or only for what comes back?
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="3">
                    <Card.Body>
                      We only charge you for data that comes back. If we can’t
                      find a match to the address submitted, you don’t get
                      charged.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <ContextAwareToggle as={Card.Header} eventKey="4">
                    How compliant is your SMS texting platform?
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="4">
                    <Card.Body>
                      {" "}
                      According to our attorneys, the SMS platform is compliant
                      for two reasons:1, We are not soliciting for services or
                      selling any goods so the vast majority of TCPA laws do not
                      apply to our texts. 2, Our system is not an autodialer and
                      there is no automation for sending mass messages without
                      human intervention.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>

                <Card>
                  <ContextAwareToggle as={Card.Header} eventKey="5">
                    Do you have an affiliate program?{" "}
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="5">
                    <Card.Body>
                      {" "}
                      We have an amazing affiliate program that pays you 20% for
                      the lifetime of the customer. For more information, please
                      contact us by using the chat bubble on the bottom.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <ContextAwareToggle as={Card.Header} eventKey="6">
                    What support do you provide?{" "}
                  </ContextAwareToggle>
                  <Accordion.Collapse eventKey="6">
                    <Card.Body>
                      {" "}
                      We strive to have the best support team in the industry.
                      Our support team can be reached via live chat and email
                      during business hours.
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </div>
            {
              <div className="col-lg-5 text-center" data-aos-duration="800">
                <img src="./Assets/Images/faq.svg" alt="faq" />
              </div>
            }
          </div>
        </div>
      </section>

      <TrustpilotReviews />
    </>
  );

  return (
    <>
      <Head>
        <title>FAQs | Finding Real Estate Deals | Batch Leads</title>
        <meta
          name="description"
          content="Have questions regarding finding real estate deals with Batch Leads. Read our FAQ page to find answers to all your questions about Batch Leads software."
        />
        <meta name="keywords" content="Finding real estate deals" />
        <link rel="canonical" href="https://batchleads.io/faq/" />
        
        <meta property="og:title" content="The All-In-One Real Estate Data & Marketing Platform | Batch Leads Software"/>
        <meta property="og:description" content="Batch Leads allows real estate agents, investors and wholesalers to find more motivated sellers, talk to more homeowners and generate more revenue." />

        <script type="application/ld+json">
          {`{
                    "@context": "https://schema.org",
                    "@type": "FAQPage",
                    "mainEntity": [{
                        "@type": "Question",
                        "name": "Do I provide my own lists and/or can I purchase lists through you?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "With the list package, you are able to pull a list directly from your platform."
                        }
                    },{
                        "@type": "Question",
                        "name": "How accurate is your vacancy data?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "Our vacancy data is the most updated data available for anyone. We are connected directly to the USPS servers and vacancy checks occur monthly as USPS updates their database."
                        }
                    },{
                        "@type": "Question",
                        "name": "How does your skip tracing compare to other skip tracing companies?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "We only use tier-1 skip tracing data and provide the best information for prospects currently available to the industry including phone numbers and emails."
                        }
                    },{
                        "@type": "Question",
                        "name": "Do I pay for all data I submit or only for what comes back?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "We only charge you for data that comes back. If we can’t find a match to the address submitted, you don’t get charged."
                        }
                    },{
                        "@type": "Question",
                        "name": "How compliant is your SMS texting platform?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "According to our attorneys, the SMS platform is compliant for two reasons:1, We are not soliciting for services or selling any goods so the vast majority of TCPA laws do not apply to our texts. 2, Our system is not an autodialer and there is no automation for sending mass messages without human intervention."
                        }
                    },{
                        "@type": "Question",
                        "name": "Do you have an affiliate program?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "We have an amazing affiliate program that pays you 20% for the lifetime of the customer. For more information, please contact us by using the chat bubble on the bottom."
                        }
                    },{
                        "@type": "Question",
                        "name": "What support do you provide?",
                        "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "We strive to have the best support team in the industry. Our support team can be reached via live chat and email during business hours."
                        }
                    }]
                    }`}
        </script>
      </Head>
      <DefaultLayout View={PageMarkup} />
    </>
  );
};

export default Faq;
