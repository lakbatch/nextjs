import React from "react";
// import serve2 from "../../Assets/Images/serve2.svg";
// import serve4 from "../../Assets/Images/serve4.svg";
// import MapComp from "../../Assets/Images/compare-map.svg";


const Compare = () => {


    return (
        <>

            <section className="serve_sec pt-5">
                <div className="container">
                    <h2 className="text-center black_text font_xbold font_40">NOT CONVINCED?</h2>

                    <div className="compare-head row align-items-center">

                        <div className="col-md-5 cloud-bg" data-aos="fade-right" data-aos-duration="1200">
                            <h3 className="font_bold head_h2 "><span className="font_xbold blue_text d-block">Compare Properties</span>
With the Power of
Nationwide MLS Access</h3>



                        </div>
                        <div className="col-md-1"></div>

                        <div className="col-md-6 mt-5" data-aos="fade-right" data-aos-duration="1200">
                            <img src="./Assets/Images/compare-map.svg" alt="map-compare" />
                        </div>


                    </div>

                    <div className="row  align-items-center">


                        <div className="col-md-6" data-aos="fade-right" data-aos-duration="1200">
                            <div className="media align-items-center serve_media mt-0">
                                <div className="media-left d-flex align-items-center justify-content-center"><img src="./Assets/Images/serve2.svg" alt="serve" /></div>
                                <div className="media-body">
                                    <h5 className="head_h3 blue_text font_med">Comparable Sales</h5>
                                    <p className="m-0 black_text">Locate all nationwide active/pending/closed properties. All 50 states are updated daily with a fully detailed 5 year listing history.</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6" data-aos="fade-right" data-aos-duration="1200">
                            <div className="media align-items-center serve_media mt-0">
                                <div className="media-left d-flex align-items-center justify-content-center"><img src="./Assets/Images/serve4.svg" alt="serve" /></div>
                                <div className="media-body">
                                    <h5 className="head_h3 blue_text font_med">Comparables All-In-One</h5>
                                    <p className="m-0 black_text">Run comparables like the pros and make decisions on the spot with simple but powerful comparable tool with built-in nationwide active/pending/closed
MLS data.</p>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </section>
        </>
    )
}

export default Compare;
;